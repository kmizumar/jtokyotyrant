/********************************************
 * Version: $Id: rdbtbl.c 11 2010-01-05 08:22:19Z koroharo $
 ********************************************/
#include "jtokyotyrant_jni_RDBTBLJni.h"
#include "myconf.h"

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    put_internal
 * Signature: ([BLjava/util/Map;)Z
 */
JNIEXPORT jboolean JNICALL Java_jtokyotyrant_jni_RDBTBLJni_put_1internal
  (JNIEnv *env, jobject self, jbyteArray jpkey, jobject jcols /*Map<byte[], byte[]>*/) {
	if (!jpkey || !jcols) {
		throwillarg(env, "jpkey or jcols is null");
		return false;
	}
	
	jboolean ic_pkey;
	jsize pkeysiz = (*env)->GetArrayLength(env, jpkey);
	jbyte *pkeybuf = (*env)->GetByteArrayElements(env, jpkey, &ic_pkey);
	if (!pkeybuf) {
		throwoutmem(env, "jpkey to jbyte");
		return false;
	}
	
	TCMAP *cols = jbytemap2tcmap(env, jcols);
	if (!cols) {
		throwoutmem(env, "jbytemap2tcmap fail");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	bool result = tcrdbtblput(rdb, pkeybuf, pkeysiz, cols);
	
	if (ic_pkey) (*env)->ReleaseByteArrayElements(env, jpkey, pkeybuf, JNI_ABORT);
	tcmapdel(cols);
	
	return result;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    putkeep_internal
 * Signature: ([BLjava/util/Map;)Z
 */
JNIEXPORT jboolean JNICALL Java_jtokyotyrant_jni_RDBTBLJni_putkeep_1internal
  (JNIEnv *env, jobject self, jbyteArray jpkey, jobject jcols) {
	if (!jpkey || !jcols) {
		throwillarg(env, "jpkey or jcols is null");
		return false;
	}
	
	jboolean ic_pkey;
	jsize pkeysiz = (*env)->GetArrayLength(env, jpkey);
	jbyte *pkeybuf = (*env)->GetByteArrayElements(env, jpkey, &ic_pkey);
	if (!pkeybuf) {
		throwoutmem(env, "jpkey to jbyte");
		return false;
	}
	
	TCMAP *cols = jbytemap2tcmap(env, jcols);
	if (!cols) {
		throwoutmem(env, "jbytemap2tcmap fail");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	bool result = tcrdbtblputkeep(rdb, pkeybuf, pkeysiz, cols);
	
	if (ic_pkey) (*env)->ReleaseByteArrayElements(env, jpkey, pkeybuf, JNI_ABORT);
	tcmapdel(cols);
	
	return result;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    putcat_internal
 * Signature: ([BLjava/util/Map;)Z
 */
JNIEXPORT jboolean JNICALL Java_jtokyotyrant_jni_RDBTBLJni_putcat_1internal
  (JNIEnv *env, jobject self, jbyteArray jpkey, jobject jcols) {
	if (!jpkey || !jcols) {
		throwillarg(env, "jpkey or jcols is null");
		return false;
	}
	
	jboolean ic_pkey;
	jsize pkeysiz = (*env)->GetArrayLength(env, jpkey);
	jbyte *pkeybuf = (*env)->GetByteArrayElements(env, jpkey, &ic_pkey);
	if (!pkeybuf) {
		throwoutmem(env, "jpkey to jbyte");
		return false;
	}
	
	TCMAP *cols = jbytemap2tcmap(env, jcols);
	if (!cols) {
		throwoutmem(env, "jbytemap2tcmap fail");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	bool result = tcrdbtblputcat(rdb, pkeybuf, pkeysiz, cols);
	
	if (ic_pkey) (*env)->ReleaseByteArrayElements(env, jpkey, pkeybuf, JNI_ABORT);
	tcmapdel(cols);
	
	return result;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    out
 * Signature: ([B)Z
 */
JNIEXPORT jboolean JNICALL Java_jtokyotyrant_jni_RDBTBLJni_out
  (JNIEnv *env, jobject self, jbyteArray jpkey) {
	if (!jpkey) {
		throwillarg(env, "jpkey is null");
		return false;
	}
	
	jboolean ic_pkey;
	jsize pkeysiz = (*env)->GetArrayLength(env, jpkey);
	jbyte *pkeybuf = (*env)->GetByteArrayElements(env, jpkey, &ic_pkey);
	if (!pkeybuf) {
		throwoutmem(env, "jpkey to jbyte");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	bool result = tcrdbtblout(rdb, pkeybuf, pkeysiz);

	if (ic_pkey) (*env)->ReleaseByteArrayElements(env, jpkey, pkeybuf, JNI_ABORT);
	return result;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    getCols
 * Signature: ([B)Ljava/util/Map;
 */
JNIEXPORT jobject /*Map<String, byte[]>*/ JNICALL Java_jtokyotyrant_jni_RDBTBLJni_getCols
  (JNIEnv *env, jobject self, jbyteArray jpkey) {
	if (!jpkey) {
		throwillarg(env, "jpkey is null");
		return false;
	}
	
	jboolean ic_pkey;
	jsize pkeysiz = (*env)->GetArrayLength(env, jpkey);
	jbyte *pkeybuf = (*env)->GetByteArrayElements(env, jpkey, &ic_pkey);
	if (!pkeybuf) {
		throwoutmem(env, "jpkey to jbyte");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	TCMAP *cols = tcrdbtblget(rdb, pkeybuf, pkeysiz);
	jobject jcols;
	if (cols) {
		jcols = tcmap2jmap(cols, env, JTTMAPTYPESTRG, JTTMAPTYPEBYTS);
		if (!jcols) {
			throwillsta(env, "tcmap2jmap fail");
			return NULL;
		}
		tcmapdel(cols);
	} else {
		jcols = NULL;
	}

	if (ic_pkey) (*env)->ReleaseByteArrayElements(env, jpkey, pkeybuf, JNI_ABORT);
	return jcols;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    setindex
 * Signature: (Ljava/lang/String;I)Z
 */
JNIEXPORT jboolean JNICALL Java_jtokyotyrant_jni_RDBTBLJni_setindex
  (JNIEnv *env, jobject self, jstring jname, jint jtype) {
	jboolean ic_name;
	const char *name = (*env)->GetStringUTFChars(env, jname, &ic_name);
	if (!name) {
		throwoutmem(env, "jname to char*");
		return false;
	}
	
	TCRDB *rdb = native_rdb(env, self);
	bool result = tcrdbtblsetindex(rdb, name, jtype);
	
	if (ic_name) (*env)->ReleaseStringUTFChars(env, jname, name);
	
	return result;
}

/*
 * Class:     jtokyotyrant_jni_RDBTBLJni
 * Method:    genuid
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_jtokyotyrant_jni_RDBTBLJni_genuid
  (JNIEnv *env, jobject self) {
	return tcrdbtblgenuid(native_rdb(env, self));
}

