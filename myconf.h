/********************************************
 * Version: $Id: myconf.h 11 2010-01-05 08:22:19Z koroharo $
 ********************************************/
#ifndef _MYCONF_H /* duplication check*/
#define _MYCONF_H

#include <jni.h>
#include <tcrdb.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include <string.h>
#include <limits.h>
#include <math.h>
#include <unistd.h>

#include <stdio.h>

#define CLSEILLARG     "java/lang/IllegalArgumentException"
#define CLSEILLSTA     "java/lang/IllegalStateException"
#define CLSEOUTMEM     "java/lang/OutOfMemoryError"
#define CLSECLSNF     "java/lang/ClassNotFoundException"
#define CLSEMAP        "java/util/Map"
#define CLSELIST       "java/util/List"
#define MTDKEYSET      "keySet"
#define MTDKEYSETSIG   "()Ljava/util/Iterator;"


/*******************************************
 * Native Tokyo Tyrant Object Access Functions
 ********************************************/
TCRDB *native_rdb
  (JNIEnv *env, jobject self);

void init_native_rdb_field
  (JNIEnv *env, jclass cls);

void set_native_rdb
  (JNIEnv *env, jobject self, TCRDB *rdb);

RDBQRY *native_qry
  (JNIEnv *env, jobject self);

void init_native_qry_field
  (JNIEnv *env, jclass cls);

void set_native_qry
  (JNIEnv *env, jobject self, RDBQRY *qry);


/*******************************************
 * Java Exception throw Functions
 ********************************************/
void throwoutmem(JNIEnv *env, const char *msg);

void throwillarg(JNIEnv *env, const char *msg);

void throwillsta(JNIEnv *env, const char *msg);

void throwclsnf(JNIEnv *env, const char *msg);

/*******************************************
 * Java Class & Instance Operate Functions
 ********************************************/
jstring getclsname(JNIEnv *env, jclass cls);

jobject new_jobject
  (JNIEnv *env, const char *clsname, const char *csig, ...);

jobject invoke_jobject_method
  (JNIEnv *env, jobject jobj, const char *mname, const char *msig, ...);

jint invoke_jint_method
  (JNIEnv *env, jobject jobj, const char *mname, const char *msig, ...);

jboolean invoke_jboolean_method
  (JNIEnv *env, jobject jobj, const char *mname, const char *msig, ...);

/*******************************************
 * Java Map & List Convert Functions
 ********************************************/
TCMAP *jbytemap2tcmap
  (JNIEnv *env, jobject jbytemap/*Map<byte[], byte[]>*/);

bool puttcmap2jbytemap
  (TCMAP *tcmap, JNIEnv *env, jobject jbytemap);

jobject tclist2jbytelist
  (JNIEnv *env, const TCLIST *tclist);

jobject tclist2jstrlist
  (JNIEnv *env, const TCLIST *tclist);

TCLIST *jbytearr2tclist
  (JNIEnv *env, jobjectArray jstrarr);

TCLIST *jstrarr2tclist
  (JNIEnv *env, jobjectArray jstrarr);

enum {
	JTTMAPTYPESTRG,
	JTTMAPTYPEBYTS
};

jobject tcmap2jmap
  (TCMAP *map, JNIEnv *env, int jmapktype, int jmapvtype);

#endif /* duplication check*/
