
# Install destinations
prefix = /usr/local
exec_prefix = ${prefix}
datarootdir = ${prefix}/share
LIBDIR = ${exec_prefix}/lib

HOMEDIR=$(HOME)

CC=gcc
CPPFLAGS=-I. -I$(INCLUDEDIR) -L$(HOMEDIR)/include -L/usr/local/include -DNDEBUG -D_GNU_SOURCE=1 -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux
CFLAGS=-std=c99 -Wall -fPIC -fsigned-char -O2
LDFLAGS = -L. -L$(LIBDIR) -L$(HOMEDIR)/lib -L/usr/local/lib
LIBS = -ltokyocabinet -lbz2 -lz -lpthread -lm -lc \
       -ltokyotyrant -lresolv -lnsl -ldl

JAVA=$(JAVA_HOME)/bin/java
JAVAC=$(JAVA_HOME)/bin/javac
JAVADOC=$(JAVA_HOME)/bin/javadoc
JAR=$(JAVA_HOME)/bin/jar
JAVAH=$(JAVA_HOME)/bin/javah

TTSERVER=/usr/local/bin/ttserver

ttjar=jtokyotyrant.jar
tthead=jtokyotyrant.h
classes=java/classes
sources=java/sources
testclasses=java/testclasses
testsources=java/test
testresources=java/testresources
testtmp=testtmp
package=jtokyotyrant
libfiles=libjtokyotyrant.so
libobjfiles=rdb.o rdbtbl.o rdbqry.o myconf.o

javafiles=$(wildcard $(sources)/$(package)/*.java \
                     $(sources)/$(package)/jni/*.java)
testfiles=$(wildcard $(testsources)/$(package)/*.java \
                     $(testsources)/$(package)/jni/*.java \
                     $(testsources)/$(package)/test/*.java)
testjavalibs=$(shell echo $(wildcard javalib/*.jar) | sed 's/ /:/g')
 
jnicls=$(package).jni.RDBJni \
          $(package).jni.RDBTBLJni \
          $(package).jni.RDBQRYJni
testcls=$(addsuffix Test, $(jnicls))

#============================
# Suffix Rules
#============================

.SUFFIXES:
.SUFFIXES: .c .o

.c.o:
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $<

#============================
# Actions
#============================

all: head $(ttjar) $(libfiles)
	@printf '\n'
	@printf 'Build completed\n'

clean: clean_j
	-rm -f jtokyotyrant_*.h *.so *.o *.jar

clean_j:
	-rm -rf $(classes)
	-rm -rf $(testclasses)


head: $(ttjar)
	$(JAVAH) -classpath $(ttjar) -jni $(jnicls)

test: all
	$(JAVA)	-classpath $(testjavalibs):$(testclasses):$(ttjar) \
	        org.junit.runner.JUnitCore \
	        $(testcls)

testserver_start:
	@-rm -rf $(testtmp)
	@-mkdir -p $(testtmp) \
	  && cd $(testtmp) \
	  && mkdir -p ./jtttestulogdir ./jtttest2ulogdir
	@-cp $(testresources)/* $(testtmp)
	cd $(testtmp) && \
	$(TTSERVER) -port 1978 \
	    -pid ./jtttest.pid \
	    -kl \
	    -ld \
	    -sid 1 \
	    -ulog ./jtttestulogdir \
		-ulim 10m \
		-ext ./jtttest.lua \
		./jtttest.tch &
	cd $(testtmp) && \
	$(TTSERVER) -port 1979 \
	    -pid ./jtttest2.pid \
	    -kl \
	    -ld \
	    -sid 2 \
	    -ulog ./jtttest2ulogdir \
		-ulim 10m \
		-ext ./jtttest.lua \
		./jtttest2.tch &
	cd $(testtmp) && \
	$(TTSERVER) -port 1988 \
		-pid ./jtttest3.pid \
		-kl \
	    -ld \
		-sid 3 \
		./jtttest.tct &

testserver_stop:
	@-cd $(testtmp) && \
	  if [ -f ./jtttest.pid ]; then \
	    kill `cat ./jtttest.pid`; \
	  fi
	@-cd $(testtmp) && \
	  if [ -f ./jtttest2.pid ]; then \
	    kill `cat ./jtttest2.pid`; \
	  fi
	@-cd $(testtmp) && \
	  if [ -f ./jtttest3.pid ]; then \
	    kill `cat ./jtttest3.pid`; \
	  fi
	@-rm -rf $(testtmp)

doc:
	@-mkdir -p doc
	@-RMFILES=$(find doc/* -type f ! -regex '.*\.svn.*'); \
	  test -n "$RMFILES" && rm -f $RMFILES
	$(JAVADOC) -d doc \
	           -package \
	           -source 5 \
	           -use \
	           -version \
	           -charset UTF-8 \
	           -docencoding UTF-8 \
	           -windowtitle "jtokyotyrant: Java binding of Tokyo Tyrant" \
	           -doctitle "jtokyotyrant: Java binding of Tokyo Tyrant" \
	           $(javafiles)

.PHONY: all clean clean_j head test testserver_start doc

#============================
# Building binaries.
#============================
$(ttjar): $(javafiles) $(testfiles)
	@mkdir -p $(classes)
	@mkdir -p $(testclasses)
	$(JAVAC) -sourcepath $(sources) -d $(classes) $(javafiles)
	cd $(classes); $(JAR) cvf ../../$@ *
	$(JAVAC) -classpath $(ttjar):$(testjavalibs) \
	         -sourcepath $(testsources) \
			 -d $(testclasses) \
			 $(testfiles)

libjtokyotyrant.so: $(libobjfiles)
	$(CC) $(CFLAGS) -shared -Wl,-soname,libjtokyotyrant.so \
	  -o $@ \
	  $(libobjfiles) $(LDFLAGS) $(LIBS)

rdb.o: jtokyotyrant_jni_RDBJni.h
rdbtbl.o: jtokyotyrant_jni_RDBTBLJni.h
rdbqry.o: jtokyotyrant_jni_RDBQRYJni.h
$(libobjfiles): myconf.h

