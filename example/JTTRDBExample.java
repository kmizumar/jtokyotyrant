import jtokyotyrant.*;

/**
 * @version $Id: JTTRDBExample.java 13 2010-01-05 09:28:22Z koroharo $
 */
public class JTTRDBExample {
    public static void main(String[] args) throws Exception {
        /* create the object */
        RDB rdb = JTT.rdbnew();  // or new jtokyotyrant.jni.RDBJni();

        /* connect to the server */
        if (!rdb.open("localhost", 1978)) {
            throw new Exception("open error:" + rdb.errmsg(rdb.ecode()));
        }

        try {
            /* store records */
            if (!rdb.put("foo", "hop") ||
                !rdb.put("bar", "step") ||
                !rdb.put("baz", "jump")) {
                throw new Exception("put error:" + rdb.errmsg(rdb.ecode()));
            }

            /* retrieve records */
            String value = rdb.get("foo");
            if (value != null) {
                System.out.println(value);
            } else {
                throw new Exception("get error:" + rdb.errmsg(rdb.ecode()));
            }

            /* delete records */
            if (!rdb.out("foo") ||
                !rdb.out("bar") ||
                !rdb.out("baz")) {
                throw new Exception("out error:" + rdb.errmsg(rdb.ecode()));
            }

        } finally {
            if (!rdb.close()) {
                throw new Exception("close error:" + rdb.errmsg(rdb.ecode()));
            }
        }
    }
}
