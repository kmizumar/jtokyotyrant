import jtokyotyrant.*;
import java.util.*;

/**
 * @version $Id: JTTRDBTBLExample.java 13 2010-01-05 09:28:22Z koroharo $
 */
public class JTTRDBTBLExample {
    public static void main(String[] args) throws Exception {
        /* create the object */
        RDBTBL rdb = JTT.rdbtblnew();  // or new jtokyotyrant.jni.RDBTBLJni();

        /* connect to the server */
        if (!rdb.open("localhost", 1988)) {
            throw new Exception("open error:" + rdb.errmsg(rdb.ecode()));
        }

        try {
            /* store a record */
            String pk1 = String.valueOf(rdb.genuid());
            Map<String, String> cols1 = new HashMap<String, String>();
            cols1.put("name", "mikio");
            cols1.put("age",  "30");
            cols1.put("lang", "ja,en,c");
            if (!rdb.put(pk1, cols1)) {
                throw new Exception("put error:" + rdb.errmsg(rdb.ecode()));
            }

            String pk2 = "12345";
            Map<String, String> cols2 = new HashMap<String, String>();
            cols2.put("name", "falcon");
            cols2.put("age",  "31");
            cols2.put("lang", "ja");
            if (!rdb.put(pk2, cols2)) {
                throw new Exception("put error:" + rdb.errmsg(rdb.ecode()));
            }

            /* search for records */
            RDBQRY qry = rdb.qrynew();
            qry.addcond("age", RDBQRY.QCNUMGE, "20");
            qry.addcond("lang", RDBQRY.QCSTROR, "ja,en");
            qry.setorder("name", RDBQRY.QOSTRASC);
            List<String> res = qry.search2();
            for (String key : res) {
                Map<String, String> cols = rdb.getCols(key);
                if (cols != null) {
                    System.out.println(key + ":" + cols);
                }
            }

            /* delete records */
            if (!rdb.out(pk1) ||
                !rdb.out(pk2)) {
                throw new Exception("out error:" + rdb.errmsg(rdb.ecode()));
            }

        } finally {
            if (!rdb.close()) {
                throw new Exception("close error:" + rdb.errmsg(rdb.ecode()));
            }
        }
    }
}
