package jtokyotyrant.jni;

import org.junit.*;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.*;
import java.io.*;

import jtokyotyrant.*;
import static jtokyotyrant.jni.JniUtil.*;
import jtokyotyrant.test.*;

/**
 * @version $Id: RDBJniTest.java 31 2010-01-08 07:13:03Z koroharo $
 */
public class RDBJniTest {

	String host = "localhost";
	int port = 1978;


	@Test
	public void initialize_finalize() {
		try {
			RDBJni rdb = new RDBJni();
//			rdb.finalize();
		} catch (Throwable th) {
			th.printStackTrace();
			fail("rdb init fail");
		}
	}

	@Test
	public void errmsg() {
		RDBJni rdb = new RDBJni();
		assertEquals("success", rdb.errmsg(RDB.ESUCCESS));
		assertEquals("invalid operation", rdb.errmsg(RDB.EINVALID));
		assertEquals("host not found", rdb.errmsg(RDB.ENOHOST));
		assertEquals("connection refused", rdb.errmsg(RDB.EREFUSED));
		assertEquals("send error", rdb.errmsg(RDB.ESEND));
		assertEquals("recv error", rdb.errmsg(RDB.ERECV));
		assertEquals("existing record", rdb.errmsg(RDB.EKEEP));
		assertEquals("no record found", rdb.errmsg(RDB.ENOREC));
		assertEquals("miscellaneous error", rdb.errmsg(RDB.EMISC));
	}
	
	@Test
	public void ecode() {
		RDBJni rdb = new RDBJni();
		assertEquals(0, rdb.ecode());
	}
	
	@Test
	public void tune() {
		RDBJni rdb = new RDBJni();
		assertEquals(true, rdb.tune(10000, RDB.TRECON));
		assertEquals(true, rdb.tune(-1, RDB.TRECON));
		assertEquals(true, rdb.tune(-1, 0));
	}
	
	@Test
	public void openAndClose() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		assertTrue(rdb.close());

		assertTrue(rdb.open("localhost" + ":" + port));
		assertTrue(rdb.close());
	}
	
	@Test
	public void putAndGetAndOut() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			
			rdb.out("hoge");
			rdb.out("foo");

			// string put
			assertTrue(rdb.put("hoge", "fuga"));
			// byte put
			assertTrue(rdb.put(s2b("foo"), s2b("bar")));
			
			// string get			
			assertEquals("fuga", rdb.get("hoge"));
			assertEquals("bar", rdb.get("foo"));
			
			// bytes get
			assertEquals("fuga", new String(rdb.get(s2b("hoge"))));
			assertEquals("bar", new String(rdb.get(s2b("foo"))));

			// string out
			assertTrue(rdb.out("foo"));
			// byte out
			assertTrue(rdb.out(s2b("hoge")));
			
			// non exist key check
			assertNull(rdb.get("hoge"));
			assertNull(rdb.get(s2b("foo")));

		} finally {
			assertTrue(rdb.close());
		}
	}
	
	@Test
	public void putkeep() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
		
			rdb.out("key1");
			rdb.out("key2");
			
			// not existing record
			assertTrue(rdb.putkeep("key1", "value11"));
			assertTrue(rdb.putkeep(s2b("key2"), s2b("value21")));
			
			// existing record string
			assertFalse(rdb.putkeep("key1", "value12"));
			assertEquals(RDB.EKEEP, rdb.ecode());
			assertEquals("value11", rdb.get("key1"));
			
			// existing record bytes
			assertFalse(rdb.putkeep(s2b("key2"), s2b("value22")));
			assertEquals(RDB.EKEEP, rdb.ecode());
			assertEquals("value21", b2s(rdb.get(s2b("key2"))));
			
			// out record
			assertTrue(rdb.out("key1"));
			assertTrue(rdb.out("key2"));
			
		} finally {
			assertTrue(rdb.close());
		}
	}
	
	@Test
	public void putcat() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
		
			rdb.out("key1");
			rdb.out("key2");
			
			// not existing record
			assertTrue(rdb.putcat("key1", "value11"));
			assertEquals("value11", rdb.get("key1"));
			
			assertTrue(rdb.putcat(s2b("key2"), s2b("value21")));
			assertEquals("value21", b2s(rdb.get(s2b("key2"))));
			
			// existing record
			assertTrue(rdb.putcat("key1", "value12"));
			assertEquals("value11value12", rdb.get("key1"));
			
			assertTrue(rdb.putcat(s2b("key2"), s2b("value22")));
			assertEquals("value21value22", b2s(rdb.get(s2b("key2"))));
			
			assertTrue(rdb.out("key1"));
			assertTrue(rdb.out("key2"));
			
		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void putshl() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
		
			rdb.out("key1");
			rdb.out("key2");
			
			// not existing record
			assertTrue(rdb.putshl("key1", "012345", 6));
			assertEquals("012345", rdb.get("key1"));
			
			assertTrue(rdb.putshl(s2b("key2"), s2b("012345"), 6));
			assertEquals("012345", b2s(rdb.get(s2b("key2"))));
			
			// existing record
			assertTrue(rdb.putshl("key1", "6789", 6));
			assertEquals("456789", rdb.get("key1"));
			
			assertTrue(rdb.putshl(s2b("key2"), s2b("6789"), 6));
			assertEquals("456789", b2s(rdb.get(s2b("key2"))));
			
			assertTrue(rdb.out("key1"));
			assertTrue(rdb.out("key2"));
			
		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void putnr() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
		
			rdb.out("key1");
			rdb.out("key2");
			
			// not existing record
			assertTrue(rdb.putnr("key1", "value11"));
			assertEquals("value11", rdb.get("key1"));
			
			assertTrue(rdb.putnr(s2b("key2"), s2b("value21")));
			assertEquals("value21", b2s(rdb.get(s2b("key2"))));
			
			// existing record
			assertTrue(rdb.putnr("key1", "value12"));
			assertEquals("value12", rdb.get("key1"));
			
			assertTrue(rdb.putnr(s2b("key2"), s2b("value22")));
			assertEquals("value22", b2s(rdb.get(s2b("key2"))));
			
			
		} finally {
			rdb.out("key1");
			rdb.out("key2");
			assertTrue(rdb.close());
		}
	}
	
	@Test
	public void mget() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.out("key2");
			
			rdb.put("key1", "value11");
			rdb.put("key2", "value21");
			
			// bytemap
			Map<byte[], byte[]> brecs = new HashMap<byte[], byte[]>();
			byte[] key1 = s2b("key1");
			brecs.put(key1, new byte[0]);
			byte[] key2 = s2b("key2");
			brecs.put(key2, new byte[0]);
			byte[] key3 = s2b("key3");
			brecs.put(key3, new byte[0]);
			
			int blen = rdb.mget(brecs);
			
			assertEquals(2, blen);
			assertEquals(blen, brecs.size());
			
			assertEquals("value11", b2s(brecs.get(key1)));
			assertEquals("value21", b2s(brecs.get(key2)));
			assertNull(brecs.get(key3));
			
			// strmap
			Map<String, String> srecs = new HashMap<String, String>();
			srecs.put("key1", "");
			srecs.put("key2", "");
			srecs.put("key3", "");
			
			int slen = rdb.mget2(srecs);
			
			assertEquals(2, slen);
			assertEquals(slen, srecs.size());
			
			assertEquals("value11", srecs.get("key1"));
			assertEquals("value21", srecs.get("key2"));
			assertNull(srecs.get("key3"));
		
		} finally {
			rdb.out("key1");
			rdb.out("key2");
			assertTrue(rdb.close());
		}
	}
	
	
	
	@Test
	public void vsiz() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.put("key1", "value11");
			
			assertEquals(7, rdb.vsiz(s2b("key1")));
			assertEquals(7, rdb.vsiz("key1"));
			
			assertEquals(-1, rdb.vsiz(s2b("notExist")));
			assertEquals(-1, rdb.vsiz("notExist"));
			
		} finally {
			rdb.out("key1");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void iterinitAndIternext() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			rdb.out("key4");
			rdb.put("key1", "1");
			rdb.put("key2", "2");
			rdb.put("key3", "3");
			rdb.put("key4", "4");
			
			// bytes			
			assertTrue(rdb.iterinit());
			
			List<byte[]> bvalues = new ArrayList<byte[]>();
			bvalues.add(rdb.iternext());
			bvalues.add(rdb.iternext());
			bvalues.add(rdb.iternext());
			bvalues.add(rdb.iternext());
			assertThat(rdb.iternext(), nullValue());

			assertThat(bvalues.size(), is(4));

			assertThat(bytesToStr(bvalues), hasItems("key1", "key2", "key3", "key4"));
			
			// string
			assertTrue(rdb.iterinit());
			
			List<String> svalues = new ArrayList<String>();
			svalues.add(rdb.iternext2());
			svalues.add(rdb.iternext2());
			svalues.add(rdb.iternext2());
			svalues.add(rdb.iternext2());
			assertThat(rdb.iternext2(), nullValue());

			assertThat(svalues.size(), is(4));

			assertThat(svalues, hasItems("key1", "key2", "key3", "key4"));

		} finally {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			rdb.out("key4");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void fwmkeys() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("abc");
			rdb.out("abC");
			rdb.out("aBC");
			rdb.put("abc", "1");
			rdb.put("abC", "2");
			rdb.put("aBC", "3");
			
			{
				List<byte[]> blist = rdb.fwmkeys(s2b("a"), 10);
				assertEquals(3, blist.size());
				assertTrue(JniUtil.bytesToStr(blist).contains("abc"));
				assertTrue(JniUtil.bytesToStr(blist).contains("abC"));
				assertTrue(JniUtil.bytesToStr(blist).contains("aBC"));

				blist = rdb.fwmkeys(s2b("ab"), 10);
				assertEquals(2, blist.size());
				assertTrue(JniUtil.bytesToStr(blist).contains("abc"));
				assertTrue(JniUtil.bytesToStr(blist).contains("abC"));

				blist = rdb.fwmkeys(s2b("a"), 2);
				assertEquals(2, blist.size());

				blist = rdb.fwmkeys(s2b("d"), 2);
				assertEquals(0, blist.size());
			}
			{
				List<String> slist = rdb.fwmkeys("a", 10);
				assertEquals(3, slist.size());
				assertTrue(slist.contains("abc"));
				assertTrue(slist.contains("abC"));
				assertTrue(slist.contains("aBC"));

				slist = rdb.fwmkeys("ab", 10);
				assertEquals(2, slist.size());
				assertTrue(slist.contains("abc"));
				assertTrue(slist.contains("abC"));

				slist = rdb.fwmkeys("a", 2);
				assertEquals(2, slist.size());

				slist = rdb.fwmkeys("d", 2);
				assertEquals(0, slist.size());
			}
			

		} finally {
			rdb.out("abc");
			rdb.out("abC");
			rdb.out("aBC");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void addint_byteArray() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			rdb.put("key3", "5");
			
			assertThat(rdb.addint(s2b("key1"), 5),  is(5));
			assertThat(rdb.addint(s2b("key1"), 10), is(15));
			assertThat(rdb.addint(s2b("key2"), 20), is(20));
			assertThat(rdb.addint(s2b("key3"), 30), not(30));

		} finally {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void adddouble_byteArray() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			rdb.put("key3", "5");
			
			assertEquals(5,  rdb.adddouble(s2b("key1"), 5), 1);
			assertEquals(15, rdb.adddouble(s2b("key1"), 10), 1);
			assertEquals(20, rdb.adddouble(s2b("key2"), 20), 1);
			assertFalse(30 == rdb.adddouble(s2b("key3"), 30));

		} finally {
			rdb.out("key1");
			rdb.out("key2");
			rdb.out("key3");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void ext() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {

			String sret = rdb.ext2("echolua", "key1", "value1", RDB.XOLCKREC);
			assertThat(sret, notNullValue());
			assertThat(sret, is("key1 value1"));
			
			assertThat(rdb.ext2("echolua"), is(" "));
			assertThat(rdb.ext2("echolua", "key1"), is("key1 "));

			byte[] bret = rdb.ext("echolua", s2b("key2"), s2b("value2"), RDB.XOLCKREC);
			assertThat(bret, notNullValue());
			assertThat(b2s(bret), is("key2 value2"));
			
			assertThat(b2s(rdb.ext("echolua")), is(" "));
			assertThat(b2s(rdb.ext("echolua", s2b("key2"))), is("key2 "));

		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void sync() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			assertTrue(rdb.sync());
		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void optimize() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			assertTrue(rdb.optimize(null));
			assertTrue(rdb.optimize("capsfawefa=1000000"));
		} finally {
			assertTrue(rdb.close());
		}
	}
	
	@Test
	public void vanishAndRnumAndSize() {
		RDBJni rdb = new RDBJni();
		
		assertThat(rdb.rnum(), is((long)0));
		assertThat(rdb.size(), is((long)0));
		
		assertTrue(rdb.open(host, port));

		final int c = 10;
		try {
			assertThat(rdb.vanish(), is(true));
			
			for (int i = 0; i < c; i++) {
				rdb.put("key" + i, "value" + i);
			}
			
			assertThat(rdb.rnum(), is((long)10));
			assertThat(rdb.size(), not((long)0));

			assertThat(rdb.vanish(), is(true));

			assertThat(rdb.rnum(), is((long)0));
			assertThat(rdb.size(), not((long)0));

		} finally {
			for (int i = 0; i < c; i++) {
				rdb.out("key" + i);
			}
			assertTrue(rdb.close());
		}
	}

	@Test
	public void copy() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			File file = new File("testtmp/copydata.tch");
			assertThat(file.exists(), is(false));
			file.deleteOnExit();

			rdb.put("key1", "5");
			rdb.put("key2", "5");
			
			assertThat(rdb.copy("./copydata.tch"), is(true));
				
			assertTrue(file.exists());
			assertTrue(file.delete());

		} finally {
			rdb.out("key1");
			rdb.out("key2");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void restore() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			String ulog = "java/testresources/restore_test.ulog";
			File file = new File(ulog);
			assertTrue(file.exists());
			try {
				// TODO: I don't understand to use 'restore'.
				rdb.restore(ulog, 0, RDB.ROCHKCON);
				assertTrue(true);
			} catch (Throwable th) {
				th.printStackTrace();
				fail("restore failed");
			}

		} finally {
			assertTrue(rdb.close());
		}
	}
	
	@Test
	public void setmst() {
		RDBJni rdb = new RDBJni();
		assertTrue(rdb.open(host, port));
		try {
			String ulog = "java/testresources/restore_test.ulog";
			File file = new File(ulog);
			assertTrue(file.exists());

			// TODO: レプレケーションの環境を用意するのが面倒なのでとりあえずエラーチェックだけ。
				
			String msthost = "localhost";
			int mstport = 1979;
				
			assertThat(rdb.setmst(msthost, mstport, 0, RDB.ROCHKCON), is(true));
			assertThat(rdb.setmst(msthost + ":" + mstport, 0, RDB.ROCHKCON), is(true));
				

		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void stat() {
		RDBJni rdb = new RDBJni();
		
		assertThat(rdb.stat(), nullValue());
		
		assertTrue(rdb.open(host, port));
		try {
			String stat;
			assertThat((stat = rdb.stat()), either(containsString("version"))
			                                   .and(containsString("libver"))
			                                   .and(containsString("cnt_get_miss")));
//			System.out.println(stat);
		} finally {
			assertTrue(rdb.close());
		}
	}

	@Test
	public void misc() {
		RDBJni rdb = new RDBJni();
		
		assertThat(rdb.stat(), nullValue());
		
		assertTrue(rdb.open(host, port));
		try {
			rdb.out("key1");
			rdb.out("key2");
			rdb.put("key1", "value1");
			rdb.put("key2", "value2");
			
			List<String> result1 = rdb.misc("get", "key1");
			assertThat(result1.size(), is(1));
			assertThat(result1, hasItem("value1"));
			
			List<byte[]> result2 = rdb.misc("get", s2b("key2"));
			assertThat(result2, notNullValue());
			assertThat(result2.size(), is(1));
			assertThat(bytesToStr(result2), hasItem("value2"));
			
			
		} finally {
			rdb.out("key1");
			rdb.out("key2");
			assertTrue(rdb.close());
		}
	}

	@Test
	public void mget2() {
		final ValueHolder<Map<byte[], byte[]>> mgetArg = ValueHolder.create();
		RDBJni rdb = new RDBJni() {
			@Override
			public int mget(Map<byte[], byte[]> recs) {
				
				mgetArg.value = new HashMap<byte[], byte[]>(recs);
				
				recs.clear();
				recs.put(s2b("hoge"), s2b("123"));
				recs.put(s2b("fuga"), s2b("456"));
				return 111;	
			}
		};
		
		Map<String, String> mget2Arg = new HashMap<String, String>();
		mget2Arg.put("hoge", "");
		mget2Arg.put("fuga", "");
		
		int count = rdb.mget2(mget2Arg);
		
		// mget2の戻り値と同じであること。
		assertEquals(111, count);
		//　mget2 で設定した内容が設定されること。
		assertEquals("123", mget2Arg.get("hoge"));
		assertEquals("456", mget2Arg.get("fuga"));
		
		// mget2の引数と、mgetの引数の数が同じであること。
		assertNotNull(mgetArg.value);
		assertEquals(mget2Arg.size(), mgetArg.value.size());
		
		// mgetの引数には、mget2
		for (byte[] mgetArgKey : mgetArg.value.keySet()) {
			byte[] mgetArgValue = mgetArg.value.get(mgetArgKey);
			
			// mget2の引数Mapのキーが、mgetにも存在すること			
			assertTrue(mget2Arg.containsKey(bytesToStr(mgetArgKey)));
			// mgetの引数Mapの値は、長さ0 のbyte配列であること。
			assertEquals(0, mgetArgValue.length);
		}
	}

	@Test
	public void addint_String() {
		final ValueHolder<byte[]> arg1 = ValueHolder.create();
		final ValueHolder<Integer> arg2 = ValueHolder.create();
		RDBJni rdb = new RDBJni() {
			@Override
			public int addint(byte[] bkey, int num) {
				arg1.value = bkey;
				arg2.value = num;
				return 111;	
			}
		};
		
		int ret = rdb.addint("hoge", 123);
		
		assertThat(ret, is(111));
		assertThat(bytesToStr(arg1.value), is("hoge"));
		assertThat( arg2.value.intValue(), is(123));
		
	}
	

	@Test
	public void adddouble_String() {
		final ValueHolder<byte[]> arg1 = ValueHolder.create();
		final ValueHolder<Double> arg2 = ValueHolder.create();
		RDBJni rdb = new RDBJni() {
			@Override
			public double adddouble(byte[] bkey, double num) {
				arg1.value = bkey;
				arg2.value = num;
				return 111;	
			}
		};
		
		double ret = rdb.adddouble("hoge", 123);
		
		assertThat(ret, is((double)111));
		assertThat(bytesToStr(arg1.value), is("hoge"));
		assertThat(arg2.value.doubleValue(), is((double)123));
		
	}

}
