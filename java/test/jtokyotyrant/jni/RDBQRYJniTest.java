package jtokyotyrant.jni;

import org.junit.*;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.*;
import java.io.*;

import jtokyotyrant.*;
import static jtokyotyrant.jni.JniUtil.*;
import jtokyotyrant.test.*;
import static jtokyotyrant.test.TestUtil.*;

/**
 * @version $Id: RDBQRYJniTest.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class RDBQRYJniTest {
	String host = "localhost";
	int port = 1988;

	RDBTBLJni tbl;
	RDBQRYJni qry;
	

	@Before
	public void before() {
		tbl = new RDBTBLJni();
		tbl.open(host, port);
		tbl.vanish();
		
		qry = (RDBQRYJni) tbl.qrynew();
	}
	
	@After	
	public void after() {
		tbl.qrydel(qry);
		tbl.vanish();
		tbl.close();
	}
	
	@Test
	public void search() {
		List<byte[]> res = qry.search();
		assertThat(res, notNullValue());
		assertThat(res.size(), is(0));

		tbl.put("key1", map("col11", "val11"));
		tbl.put("key2", map("col21", "val21"));
		tbl.put("key3", map("col31", "val31"));

		res = qry.search();
		assertThat(res.size(), is(3));
		assertThat(bytesToStr(res), hasItems("key1", "key2", "key3"));
	}

	@Test
	public void searchget() {
		List<Map<String, byte[]>> res = qry.searchget();
		assertThat(res, notNullValue());
		assertThat(res.size(), is(0));

		tbl.put("key1", map("col1", "val11").map("col2", "val21"));
		tbl.put("key2", map("col1", "val12").map("col2", "val22"));
		tbl.put("key3", map("col1", "val13").map("col2", "val23"));

		res = qry.searchget();
		assertThat(res.size(), is(3));
		
		for (Map<String, byte[]> cols : res) {
			assertThat(cols.size(), is(3));
			assertThat(cols.keySet(), hasItems("", "col1", "col2"));
			assertThat(b2s(cols.get("")),     either(is("key1")).or(is("key2")).or(is("key3")));
			assertThat(b2s(cols.get("col1")), either(is("val11")).or(is("val12")).or(is("val13")));
			assertThat(b2s(cols.get("col2")), either(is("val21")).or(is("val22")).or(is("val23")));
		}
	}

	@Test
	public void searchget2() {
		List<Map<String, String>> res = qry.searchget2();
		assertThat(res, notNullValue());
		assertThat(res.size(), is(0));

		tbl.put("key1", map("col1", "val11").map("col2", "val21"));
		tbl.put("key2", map("col1", "val12").map("col2", "val22"));
		tbl.put("key3", map("col1", "val13").map("col2", "val23"));

		res = qry.searchget2();
		assertThat((long)res.size(), both(is(3L)).and(is(tbl.rnum())));
		
		for (Map<String, String> cols : res) {
			assertThat(cols.size(), is(3));
			assertThat(cols.keySet(), hasItems("", "col1", "col2"));
			assertThat(cols.get(""),     either(is("key1")).or(is("key2")).or(is("key3")));
			assertThat(cols.get("col1"), either(is("val11")).or(is("val12")).or(is("val13")));
			assertThat(cols.get("col2"), either(is("val21")).or(is("val22")).or(is("val23")));
		}
	}

	@Test
	public void searchout() {
		assertThat(tbl.rnum(), is(0L));

		assertThat(qry.searchout(), is(true));

		tbl.put("key1", map("col1", "val11"));
		tbl.put("key2", map("col1", "val12"));
		tbl.put("key3", map("col1", "val13"));
		
		assertThat(tbl.rnum(), is(3L));
		
		assertThat(qry.searchout(), is(true));

		assertThat(tbl.rnum(), is(0L));
	}

	@Test
	public void searchcount() {
		assertThat(tbl.rnum(), is(0L));
		assertThat(qry.searchcount(), is(0));

		tbl.put("key1", map("col1", "val11"));
		tbl.put("key2", map("col1", "val12"));
		tbl.put("key3", map("col1", "val13"));

		assertThat(qry.searchcount(), is(3));
	}
	
	@Test
	public void hint() {
		qry.search();
	
		String hint = qry.hint();
//		System.out.println("+++ qryhint:" + hint);
		assertThat(hint, notNullValue());
	}

	@Test
	public void addcond_setorder() {
		tbl.put("key1", map("col1", "4"));
		tbl.put("key2", map("col1", "3"));
		tbl.put("key3", map("col1", "2"));
		tbl.put("key4", map("col1", "1"));
		
		qry.setorder("", RDBQRY.QOSTRASC);
		List<String> list = qry.search2();
		assertThat(list.get(0), is("key1"));
		assertThat(list.get(1), is("key2"));
		assertThat(list.get(2), is("key3"));
		assertThat(list.get(3), is("key4"));

		qry.setorder("", RDBQRY.QOSTRDESC);
		list = qry.search2();
		assertThat(list.get(0), is("key4"));
		assertThat(list.get(1), is("key3"));
		assertThat(list.get(2), is("key2"));
		assertThat(list.get(3), is("key1"));
		
		qry.setorder("col1", RDBQRY.QONUMASC);
		list = qry.search2();
		assertThat(list.get(0), is("key4"));
		assertThat(list.get(1), is("key3"));
		assertThat(list.get(2), is("key2"));
		assertThat(list.get(3), is("key1"));

		qry.setorder("col1", RDBQRY.QONUMDESC);
		list = qry.search2();
		assertThat(list.get(0), is("key1"));
		assertThat(list.get(1), is("key2"));
		assertThat(list.get(2), is("key3"));
		assertThat(list.get(3), is("key4"));
	}

	
	@Test
	public void addcond_streq() {
		qry.addcond("", RDBQRY.QCSTREQ | RDBQRY.QCNOIDX, "key2");
		
		tbl.put("key1", map("col1", "val11"));

		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "val11"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_streq_negate() {
		qry.addcond("", RDBQRY.QCSTREQ | RDBQRY.QCNEGATE, "key2");

		tbl.put("key1", map("col1", "val11"));
		tbl.put("key2", map("col1", "val11"));

		List<String> list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key1"));
	}

	@Test
	public void addcond_strinc() {
		qry.addcond("col1", RDBQRY.QCSTRINC, "def");

		tbl.put("key1", map("col1", "abcabcabc"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "abcdefabc"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_strbw() {
		qry.addcond("col1", RDBQRY.QCSTRBW, "def");

		tbl.put("key1", map("col1", "abcdefabc"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "defabcabc"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_strew() {
		qry.addcond("col1", RDBQRY.QCSTREW, "def");

		tbl.put("key1", map("col1", "abcdefabc"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "abcabcdef"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_strand() {
		qry.addcond("col1", RDBQRY.QCSTRAND, "dd,ee,ff");

		tbl.put("key1", map("col1", "aa,bb,cc,dd,ee"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "aa,cc,ee,dd,ff"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_stror() {
		qry.addcond("col1", RDBQRY.QCSTROR, "dd,ee,ff");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "aa,bb,cc"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "aa,cc,ff"));
		tbl.put("key3", map("col1", "aa,cc,ee"));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}

	@Test
	public void addcond_stroreq() {
		qry.addcond("col1", RDBQRY.QCSTROREQ, "dd,ee,ff");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "aa"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "dd"));
		tbl.put("key3", map("col1", "ff"));
		tbl.put("key4", map("col1", "dd,ee"));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}

	@Test
	public void addcond_strrx() {
		qry.addcond("col1", RDBQRY.QCSTRRX, "^a[0-9]b$");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "aa1bb"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "a2b"));
		tbl.put("key3", map("col1", "a9b"));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}

	@Test
	public void addcond_numeq() {
		qry.addcond("col1", RDBQRY.QCNUMEQ, "100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "10000"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "100"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_numgt() {
		qry.addcond("col1", RDBQRY.QCNUMGT, "100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "100"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "101"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}
	@Test
	public void addcond_numge() {
		qry.addcond("col1", RDBQRY.QCNUMGE, "100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "99"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "100"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_numlt() {
		qry.addcond("col1", RDBQRY.QCNUMLT, "100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "100"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "99"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_numle() {
		qry.addcond("col1", RDBQRY.QCNUMLE, "100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "101"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "100"));
		
		list = qry.search2();
		assertThat(list.size(), is(1));
		assertThat(list.get(0), is("key2"));
	}

	@Test
	public void addcond_numbt() {
		qry.addcond("col1", RDBQRY.QCNUMBT, "50,100");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "49"));
		tbl.put("key2", map("col1", "101"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key3", map("col1", "50"));
		tbl.put("key4", map("col1", "70"));
		tbl.put("key5", map("col1", "100"));
		
		list = qry.search2();
		assertThat(list.size(), is(3));
		assertThat(list.get(0), is("key3"));
		assertThat(list.get(1), is("key4"));
		assertThat(list.get(2), is("key5"));
	}

	@Test
	public void addcond_numoreq() {
		qry.addcond("col1", RDBQRY.QCNUMOREQ, "10,20,30");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "40"));
		tbl.put("key2", map("col1", "50"));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key3", map("col1", "10"));
		tbl.put("key4", map("col1", "30"));
		tbl.put("key5", map("col1", "20"));
		
		list = qry.search2();
		assertThat(list.size(), is(3));
		assertThat(list.get(0), is("key3"));
		assertThat(list.get(1), is("key4"));
		assertThat(list.get(2), is("key5"));
	}

	@Test
	public void addcond_ftsph() {
		tbl.setindex("col1", RDBTBL.ITQGRAM);
	
		qry.addcond("col1", RDBQRY.QCFTSPH, "fox");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "The quick brown bear jumps over the lazy dog."));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "The quick brown fox jumps over the lazy dog."));
		tbl.put("key3", map("col1", "The quick brown FOX jumps over the lazy dog."));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}

	@Test
	public void addcond_ftsand() {
		tbl.setindex("col1", RDBTBL.ITQGRAM);
	
		qry.addcond("col1", RDBQRY.QCFTSAND, "fox,dog");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "The quick brown bear jumps over the lazy dog."));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "The quick brown fox jumps over the lazy dog."));
		tbl.put("key3", map("col1", "The quick brown FOX jumps over the lazy doG."));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}

	@Test
	public void addcond_ftsor() {
		tbl.setindex("col1", RDBTBL.ITQGRAM);
	
		qry.addcond("col1", RDBQRY.QCFTSOR, "fox,dog");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "The quick brown rat jumps over the lazy cat."));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "The quick brown fox jumps over the lazy cat."));
		tbl.put("key3", map("col1", "The quick brown rat jumps over the lazy dog."));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}
	
	/**
	 * 複合検索式においては、空白で区切って複数のトークンを指定すると、
	 * その全てのトークンを含むというAND条件で検索できます。空白および
	 * 「&&」で区切っても同じ意味になります。空白および「||」で区切ると、
	 * 両辺のトークンのどちらかを含むというOR条件で検索できます。
	 * トークンに空白を含めたい場合は「""」で括ります。演算子の結合優先
	 * 順位は「""」「||」「&&」の順になります。同一順位の演算子は左結合
	 * で評価されます。
	 */
	@Test
	public void addcond_ftsex() {
		tbl.setindex("col1", RDBTBL.ITQGRAM);
	
		qry.addcond("col1", RDBQRY.QCFTSEX, "fox && dog && \" over \"");
		qry.setorder("", RDBQRY.QOSTRASC);

		tbl.put("key1", map("col1", "The quick brown bear jumps over the lazy dog."));
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(0));

		tbl.put("key2", map("col1", "The quick brown foxes jumps over the lazy dog."));
		tbl.put("key3", map("col1", "The quick brown FOX jumps over the lazy DOG."));
		tbl.put("key4", map("col1", "The quick brown FOX jumps overthe lazy DOG."));
		
		list = qry.search2();
		assertThat(list.size(), is(2));
		assertThat(list.get(0), is("key2"));
		assertThat(list.get(1), is("key3"));
	}
	
	@Test
	public void setlimit() {
		for (int i = 0; i < 10; i++) {
			tbl.put("key"+i, map("col1", "val1"));
		}
		qry.setlimit(3, 0);
		qry.setorder("", RDBQRY.QOSTRASC);
		
		List<String> list = qry.search2();
		assertThat(list.size(), is(3));
		assertThat(list.get(0), is("key0"));
		assertThat(list.get(2), is("key2"));

		qry.setlimit(3, 5);
		list = qry.search2();
		assertThat(list.size(), is(3));
		assertThat(list.get(0), is("key5"));
		assertThat(list.get(2), is("key7"));

		qry.setlimit(-1, 5);
		list = qry.search2();
		assertThat(list.size(), is(5));
		assertThat(list.get(0), is("key5"));
		assertThat(list.get(4), is("key9"));
	}
	
	@Test
	public void metasearch() {
		for (int i = 0; i < 10; i++) {
			tbl.put("key"+i, map("col", "val" + i));
		}

		List<String> list = qry.metasearch2(new RDBQRY[0], RDBQRY.MSUNION);
		assertThat(list, notNullValue());
		assertThat(list.size(), is(0));


		RDBQRY qry1 = tbl.qrynew();
		qry1.addcond("col", RDBQRY.QCSTREQ, "val0");
		qry1.setorder("col", RDBQRY.QOSTRASC);

		RDBQRY qry2 = tbl.qrynew();
		qry2.addcond("", RDBQRY.QCSTREQ, "key9");

		RDBQRY qry3 = tbl.qrynew();
		qry3.addcond("col", RDBQRY.QCSTRRX, "^val[4-6]$");
		
		list = qry.metasearch2(new RDBQRY[]{qry1, qry2, qry3}, RDBQRY.MSUNION);
		assertThat(list.size(), is(5));
		assertThat(list, hasItems("key0", "key4", "key5", "key6", "key9"));
		assertThat(list.get(0), is("key0"));
		assertThat(list.get(4), is("key9")); // ??? key9
		
		
	}
	
	@Test
	public void parsearch() {
		for (int i = 0; i < 10; i++) {
			tbl.put("key"+i, map("col", "val" + i));
		}

		List<Map<String, String>> list = qry.parasearch2(new RDBQRY[0]);
		assertThat(list, notNullValue());
		assertThat(list.size(), is(0));
		
		RDBQRY qry1 = tbl.qrynew();
		qry1.addcond("col", RDBQRY.QCSTREQ, "val0");
		qry1.setorder("col", RDBQRY.QOSTRASC);

		RDBQRY qry2 = tbl.qrynew();
		qry2.addcond("", RDBQRY.QCSTREQ, "key9");

		RDBQRY qry3 = tbl.qrynew();
		qry3.addcond("col", RDBQRY.QCSTRRX, "^val[4-6]$");
		
		list = qry.parasearch2(new RDBQRY[]{qry1, qry2, qry3});
		assertThat(list.size(), is(5));

		assertThat(list.get(0).get(""), is("key0"));
		assertThat(list.get(4).get(""), is("key9"));
		
	}
}

