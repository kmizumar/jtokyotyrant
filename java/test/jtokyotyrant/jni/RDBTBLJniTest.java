package jtokyotyrant.jni;

import org.junit.*;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.*;
import java.io.*;

import jtokyotyrant.*;
import static jtokyotyrant.jni.JniUtil.*;
import jtokyotyrant.test.*;
import static jtokyotyrant.test.TestUtil.*;

/**
 * @version $Id: RDBTBLJniTest.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class RDBTBLJniTest {

	String host = "localhost";
	int port = 1988;
	
	RDBTBLJni tbl;
	@Before
	public void before() {
		tbl = new RDBTBLJni();
		assertThat(tbl.open(host, port), is(true));
		assertThat(tbl.vanish(), is(true));
	}
	
	@After
	public void after() {
		assertThat(tbl.vanish(), is(true));
		assertThat(tbl.close(), is(true));
	}

	@Test
	public void put() {
		// bytes
		assertThat(tbl.put(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col11", s2b("val11"));
			put("col12", s2b("val12"));
		}}), is(true));
		
		Map<String, byte[]> cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(2));
		assertThat(b2s(cols1.get("col11")), is("val11"));
		assertThat(b2s(cols1.get("col12")), is("val12"));

		assertThat(tbl.put(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col11", s2b("val11-a"));
			put("col13", s2b("val13"));
		}}), is(true));

		cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(2));
		assertThat(b2s(cols1.get("col11")), is("val11-a"));
		assertThat(b2s(cols1.get("col13")), is("val13"));
		assertThat(cols1.get("col12"), nullValue());
		
		// string
		assertThat(tbl.put("key2", new HashMap<String, String>() {{
			put("col21", "val21");
			put("col22", "val22");
		}}), is(true));

		Map<String, String> cols2 = tbl.getCols("key2");
		assertThat(cols2, notNullValue());
		assertThat(cols2.size(), is(2));
		assertThat(cols2.get("col21"), is("val21"));
		assertThat(cols2.get("col22"), is("val22"));
		
		assertThat(tbl.getCols(s2b("key3")), nullValue());
		assertThat(tbl.getCols("key3"), nullValue());
		
	}

	@Test
	public void putkeep() {
		// bytes
		assertThat(tbl.putkeep(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col11", s2b("val11"));
			put("col12", s2b("val12"));
		}}), is(true));
		
		Map<String, byte[]> cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(2));
		assertThat(b2s(cols1.get("col11")), is("val11"));
		assertThat(b2s(cols1.get("col12")), is("val12"));

		assertThat(tbl.putkeep(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col13", s2b("val13"));
		}}), is(false));
		assertThat(tbl.ecode(), is(RDB.EKEEP));
		
		cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(2));
		assertThat(b2s(cols1.get("col11")), is("val11"));
		assertThat(b2s(cols1.get("col12")), is("val12"));
		
		// string		
		assertThat(tbl.putkeep("key2", new HashMap<String, String>() {{
			put("col21", "val21");
			put("col22", "val22");
		}}), is(true));

		Map<String, String> cols2 = tbl.getCols("key2");
		assertThat(cols2, notNullValue());
		assertThat(cols2.size(), is(2));
		assertThat(cols2.get("col21"), is("val21"));
		assertThat(cols2.get("col22"), is("val22"));
	}

	@Test
	public void putcat() {
		// bytes
		assertThat(tbl.putcat(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col11", s2b("val11"));
			put("col12", s2b("val12"));
		}}), is(true));
		
		Map<String, byte[]> cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(2));
		assertThat(b2s(cols1.get("col11")), is("val11"));
		assertThat(b2s(cols1.get("col12")), is("val12"));

		assertThat(tbl.putcat(s2b("key1"), new HashMap<String, byte[]>() {{
			put("col11", s2b("val11-a"));
			put("col13", s2b("val13"));
		}}), is(true));
		cols1 = tbl.getCols(s2b("key1"));
		assertThat(cols1, notNullValue());
		assertThat(cols1.size(), is(3));
		assertThat(b2s(cols1.get("col11")), is("val11"));
		assertThat(b2s(cols1.get("col12")), is("val12"));
		assertThat(b2s(cols1.get("col13")), is("val13"));
		
		// string		
		assertThat(tbl.putcat("key2", new HashMap<String, String>() {{
			put("col21", "val21");
			put("col22", "val22");
		}}), is(true));

		Map<String, String> cols2 = tbl.getCols("key2");
		assertThat(cols2, notNullValue());
		assertThat(cols2.size(), is(2));
		assertThat(cols2.get("col21"), is("val21"));
		assertThat(cols2.get("col22"), is("val22"));
	}
	
	@Test
	public void out() {
		assertThat(tbl.put("key1", new HashMap<String, String>()), is(true));
		Map<String, String> cols1 = tbl.getCols("key1");
		assertThat(cols1, notNullValue());
		assertThat(tbl.out(s2b("key1")), is(true));
		cols1 = tbl.getCols("key1");
		assertThat(cols1, nullValue());

		assertThat(tbl.put("key2", new HashMap<String, String>()), is(true));
		Map<String, String> cols2 = tbl.getCols("key2");
		assertThat(cols2, notNullValue());
		assertThat(tbl.out("key2"), is(true));
		cols2 = tbl.getCols("key2");
		assertThat(cols2, nullValue());
	}
	@Test
	public void setindex() {
		try {
			assertThat(tbl.setindex("", RDBTBL.ITDECIMAL), is(true));
			assertThat(tbl.setindex("", RDBTBL.ITLEXICAL), is(true));
			assertThat(tbl.setindex("", RDBTBL.ITLEXICAL | RDBTBL.ITKEEP), is(false));
			assertThat(tbl.ecode(), is(RDBTBL.EMISC));
			assertThat(tbl.setindex("", RDBTBL.ITOPT), is(true));

			assertThat(tbl.setindex("col11", RDBTBL.ITLEXICAL | RDBTBL.ITKEEP), is(true));

		} finally {
			assertThat(tbl.setindex("", RDBTBL.ITVOID), is(true));
			assertThat(tbl.setindex("col11", RDBTBL.ITVOID), is(true));
		}
	}
	
	@Test
	public void genuid() {
		assertThat(tbl.genuid(), not(-1L));
	}
	
	@Test
	public void qrynewAndQrydel() {
		RDBQRY qry = tbl.qrynew();
		assertThat(qry, instanceOf(RDBQRYJni.class));
		
		assertThat(tbl.qrydel(qry), is(true));
	}
}

