package jtokyotyrant.test;

import java.util.HashMap;

/**
 * @version $Id: TestUtil.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class TestUtil {
	private TestUtil() {
		super();
	}
	
	public static <K, V> MapBuilder<K, V> map(K key, V value) {
		return new MapBuilder<K, V>().map(key, value);
	}
	
	public static class MapBuilder<K, V> extends HashMap<K, V> {
		private MapBuilder() {}
		public MapBuilder<K, V> map(K key, V value) {
			this.put(key, value);
			return this;
		}
	}
}
