package jtokyotyrant.test;

/**
 * @version $Id: ValueHolder.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class ValueHolder<T> {
	public T value;
	private ValueHolder() {
		super();
	}
	
	public static <RET> ValueHolder<RET> create() {
		return new ValueHolder<RET>();
	}
}

