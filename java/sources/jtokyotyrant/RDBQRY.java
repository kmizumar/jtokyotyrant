package jtokyotyrant;

import java.util.List;
import java.util.Map;

/**
 * @version $Id: RDBQRY.java 35 2010-01-19 08:07:53Z koroharo $
 */
public interface RDBQRY {
	/** query condition: string is equal to */
	int QCSTREQ  	=  	0;
	/** query condition: string is included in */
	int QCSTRINC 	= 	1;
	/** query condition: string begins with */
	int QCSTRBW 	= 	2;
	/** query condition: string ends with */
	int QCSTREW 	= 	3;
	/** query condition: string includes all tokens in */
	int QCSTRAND 	= 	4;
	/** query condition: string includes at least one token in */
	int QCSTROR 	= 	5;
	/** query condition: string is equal to at least one token in */
	int QCSTROREQ 	= 	6;
	/** query condition: string matches regular expressions of */
	int QCSTRRX 	= 	7;
	/** query condition: number is equal to */
	int QCNUMEQ 	= 	8;
	/** query condition: number is greater than */
	int QCNUMGT 	= 	9;
	/** query condition: number is greater than or equal to */
	int QCNUMGE 	= 	10;
	/** query condition: number is less than */
	int QCNUMLT 	= 	11;
	/** query condition: number is less than or equal to */
	int QCNUMLE 	= 	12;
	/** query condition: number is between two tokens of */
	int QCNUMBT 	= 	13;
	/** query condition: number is equal to at least one token in */
	int QCNUMOREQ 	= 	14;
	/** query condition: full-text search with the phrase of */
	int QCFTSPH 	= 	15;
	/** query condition: full-text search with all tokens in */
	int QCFTSAND 	= 	16;
	/** query condition: full-text search with at least one token in */
	int QCFTSOR 	= 	17;
	/** query condition: full-text search with the compound expression of */
	int QCFTSEX 	= 	18;
	/** query condition: negation flag */
	int QCNEGATE 	= 	1 << 24;
	/** query condition: no index flag */
	int QCNOIDX 	= 	1 << 25;
	/** order type: string ascending */
	int QOSTRASC 	= 	0;
	/** order type: string descending */
	int QOSTRDESC 	= 	1;
	/** order type: number ascending */
	int QONUMASC 	= 	2;
	/** order type: number descending */
	int QONUMDESC 	= 	3;
	/** set operation type: union */
	int MSUNION 	= 	0;
	/** set operation type: intersection */
	int MSISECT 	= 	1;
	/** set operation type: difference */
	int MSDIFF 	= 	2;
	
	/**
	 * This method is used in order to add a narrowing condition to a query object.
	 * <p>
	 * Operatoin types are listed as below:
	 * <ul>
	 * <li>for string:
	 * <ul>
	 * <li>{@link #QCSTREQ} for string which is equal to the expression. 
	 * <li>{@link #QCSTRINC} for string which is included in the expression.
	 * <li>{@link #QCSTRBW} for string which begins with the expression.
	 * <li>{@link #QCSTREW} for string which ends with the expression.
	 * <li>{@link #QCSTRAND} for string which includes all tokens in the expression.
	 * <li>{@link #QCSTROR} for string which includes at least one token in the expression.
	 * <li>{@link #QCSTROREQ} for string which is equal to at least one token in the expression.
	 * <li>{@link #QCSTRRX} for string which matches regular expressions of the expression.
	 * </ul>
	 * <li>for number:
	 * <ul>
	 * <li>{@link #QCNUMEQ} for number which is equal to the expression.
	 * <li>{@link #QCNUMGT} for number which is greater than the expression.
	 * <li>{@link #QCNUMGE} for number which is greater than or equal to the expression.
	 * <li>{@link #QCNUMLT} for number which is less than the expression.
	 * <li>{@link #QCNUMLE} for number which is less than or equal to the expression.
	 * <li>{@link #QCNUMBT} for number which is between two tokens of the expression.
	 * <li>{@link #QCNUMOREQ} for number which is equal to at least one token in the expression.
	 * </ul>
	 * <li>for full-text search:
	 * <ul>
	 * <li>{@link #QCFTSPH} for full-text search with the phrase of the expression.
	 * <li>{@link #QCFTSAND} for full-text search with all tokens in the expression.
	 * <li>{@link #QCFTSOR} for full-text search with at least one token in the expression.
	 * <li>{@link #QCFTSEX} for full-text search with the compound expression. 
	 * </ul>
	 * </ul>
	 * All operations can be flagged by bitwise-or: 
	 *         {@link #QCNEGATE} for negation, 
	 *         {@link #QCNOIDX} for using no index.
	 * </p>
	 * @param name specifies the name of a column. An empty string means the 
	 *           primary key.
	 * @param op specifies an operation type
	 * @param expr specifies an operand exression.
	 */
	void addcond(String name, int op, String expr);
	/**
	 * This method is used in order to set the order of a query object.
	 * <p>
	 * Ordet types are listed as below:
	 * <ul>
	 * <li>{@link #QOSTRASC} for string ascending.
	 * <li>{@link #QOSTRDESC} for string descending.
	 * <li>{@link #QONUMASC} for number ascending.
	 * <li>{@link #QONUMDESC} for number descending.
	 * </ul>
	 * </p>
	 * @param name specifies the name of a column. An empty string means 
	 *          the primary key.
	 * @param type specifies the order type.
	 */
	void setorder(String name, int type);
	/**
	 * This method is used in order to set the limit number of records of 
	 * the result of a query object.
	 * @param max specifies the maximum number of records of the result.
	 *           If it is negative, no limit is specified.
	 * @param skip specifies the number of skipped records of the result.
	 *           If it is not more than 0, no record is skipped.
	 */
	void setlimit(int max, int skip);
	/**
	 * This method is used in order to execute the search of a query object.
	 * @return The return value is a list object of the primary keys of the 
	 *         corresponding records. This function does never fail. 
	 *         It returns an empty list even if no record corresponds.
	 */
	List<byte[]> search();
	/**
	 * This method is used in order to execute the search of a query object.
	 * @return The return value is a list object of the primary keys of the 
	 *         corresponding records. This function does never fail. 
	 *         It returns an empty list even if no record corresponds.
	 */
	List<String> search2();
	/**
	 * This method is used in order to remove each record corresponding to a 
	 * query object.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean searchout();
	/**
	 * This method is used in order to get records corresponding to the search 
	 * of a query object.
	 * <p>
	 * The primary key in the return value is corresponded to '\0'.
	 * </p>
	 * @return The return value is a list object of a map object containing 
	 *         columns of the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds. 
	 */
	List<Map<String, byte[]>> searchget();
	/**
	 * This method is used in order to get records corresponding to the search 
	 * of a query object.
	 * <p>
	 * The primary key in the return value is corresponded to '\0'.
	 * </p>
	 * @return The return value is a list object of a map object containing 
	 *         columns of the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds. 
	 */
	List<Map<String, String>> searchget2();
	/**
	 * This method is used in order to get the count of corresponding records 
	 * of a query object.
	 * @return The return value is the count of corresponding records or 0 on failure.
	 */	
	int searchcount();
	/**
	 * This method is used in order to get the hint string of a query object.
	 * @return The return value is the hint string. 
	 */
	String hint();
	
	/**
	 * This method is used in order to retrieve records with multiple query 
	 * objects and get the set of the result.
	 * <p>
	 * If the first query object has the order setting, the result array is sorted by the order.
	 * </p>
	 * <p>
	 * <ul>
	 * <li>{@link #MSUNION} for the union set.
	 * <li>{@link #MSISECT} for the intersection set.
	 * <li>{@link #MSDIFF} for the difference set.
	 * </ul>
	 * </p>
	 * @param qrys specifies an array of the query objects.
	 * @param type specifies a set operation type.
	 * @return The return value is a list object of the primary keys of 
	 *         the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds.
	 */
	List<byte[]> metasearch(RDBQRY[] qrys, int type);
	/**
	 * This method is used in order to retrieve records with multiple query 
	 * objects and get the set of the result.
	 * <p>
	 * If the first query object has the order setting, the result array is sorted by the order.
	 * </p>
	 * <p>
	 * <ul>
	 * <li>{@link #MSUNION} for the union set.
	 * <li>{@link #MSISECT} for the intersection set.
	 * <li>{@link #MSDIFF} for the difference set.
	 * </ul>
	 * </p>
	 * @param qrys specifies an array of the query objects.
	 * @param type specifies a set operation type.
	 * @return The return value is a list object of the primary keys of 
	 *         the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds.
	 */
	List<String> metasearch2(RDBQRY[] qrys, int type);

	/**
	 * This method is used in order to search records for multiple servers in parallel.
	 * <p>
	 * The primary key in the return value is corresponded to '\0'.
	 * </p>
	 * @param qrys specifies an array of the query objects.
	 * @return The return value is a list object of a map object containing 
	 *         columns of the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds.
	 */
	List<Map<String, byte[]>> parasearch(RDBQRY[] qrys);
	/**
	 * This method is used in order to search records for multiple servers in parallel.
	 * <p>
	 * The primary key in the return value is corresponded to '\0'.
	 * </p>
	 * @param qrys specifies an array of the query objects.
	 * @return The return value is a list object of a map object containing 
	 *         columns of the corresponding records.
	 *         This function does never fail. It returns an empty list even if 
	 *         no record corresponds.
	 */
	List<Map<String, String>> parasearch2(RDBQRY[] qrys);
	
}
