package jtokyotyrant.protocol;

import jtokyotyrant.RDB;

public class Out extends Command<Boolean> {
    private Object key;

    public Out(RDB rdb, Object key) {
        super(rdb);
        this.key = key;
    }

    public Boolean call() throws Exception {
        if (key instanceof byte[]) {
            return rdb.out((byte[]) key);
        }
        else if (key instanceof String) {
            return rdb.out((String) key);
        }
        else {
            return false;
        }
    }
}
