package jtokyotyrant.protocol;

import java.util.concurrent.Callable;

import jtokyotyrant.RDB;

public abstract class Command<T> implements Callable<T> {
    protected final RDB rdb;

    public Command(RDB rdb) {
        this.rdb = rdb;
    }

    public CommandFuture<T> submit() {
        return new CommandFuture<T>(this);
    }

    // public <T> call() throws Exception {
    //     return new T();
    // }
}
