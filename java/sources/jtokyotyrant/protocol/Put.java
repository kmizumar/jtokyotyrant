package jtokyotyrant.protocol;

import jtokyotyrant.RDB;

public class Put extends Command<Boolean> {
    private Object key;
    private Object value;

    public Put(RDB rdb, Object key, Object value) {
        super(rdb);
        this.key = key;
        this.value = value;
    }

    public Boolean call() throws Exception {
        if (key instanceof byte[] && value instanceof byte[]) {
            return rdb.put((byte[]) key, (byte[]) value);
        }
        else if (key instanceof String && value instanceof String) {
            return rdb.put((String) key, (String) value);
        }
        else {
            return false;
        }
    }
}
