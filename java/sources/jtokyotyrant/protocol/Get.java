package jtokyotyrant.protocol;

import jtokyotyrant.RDB;

public class Get extends Command<Object> {
    private Object key;

    public Get(RDB rdb, Object key) {
        super(rdb);
        this.key = key;
    }

    public Object call() throws Exception {
        if (key instanceof byte[]) {
            return rdb.get((byte[]) key);
        }
        else if (key instanceof String) {
            return rdb.get((String) key);
        }
        else {
            return null;
        }
    }
}
