package jtokyotyrant.protocol;

import java.util.ArrayList;
import java.util.List;

import jtokyotyrant.RDB;

public class Fwmkeys extends Command<Object> {
    private byte[] prefixByteArray = null;
    private String prefixString = null;
    private int max;

    public Fwmkeys(RDB rdb, byte[] prefix, int max) {
        super(rdb);
        this.prefixByteArray = prefix;
        this.max = max;
    }

    public Fwmkeys(RDB rdb, String prefix, int max) {
        super(rdb);
        this.prefixString = prefix;
        this.max = max;
    }

    public Object call() throws Exception {
        if (prefixByteArray != null) {
            return rdb.fwmkeys(prefixByteArray, max);
        }
        if (prefixString != null) {
            return rdb.fwmkeys(prefixString, max);
        }
        return null;
    }
}
