package jtokyotyrant.jni;

/**
 * @version $Id: Loader.java 10 2010-01-05 08:20:53Z koroharo $
 */
class Loader {
	private static boolean loaded = false;
	public static void load() {
		if (!loaded) {
			System.loadLibrary("jtokyotyrant");
			loaded = true;
		}
	}
}
