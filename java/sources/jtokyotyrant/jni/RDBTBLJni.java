package jtokyotyrant.jni;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import jtokyotyrant.RDBTBL;
import jtokyotyrant.RDBQRY;

/**
 * @version $Id: RDBTBLJni.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class RDBTBLJni extends RDBJni implements RDBTBL {
	
	private List<RDBQRYJni> qrys = new ArrayList<RDBQRYJni>();

	public RDBTBLJni() {
		super();
	}

	@Override public boolean close() {
		qrys.clear();
		return super.close();
	}
	
	@Override public boolean put(byte[] pkey, Map<String, byte[]> cols) {
		return put_internal(pkey, 
			JniUtil.strToBytes(cols));
	}
	@Override public boolean put(String pkey, Map<String, String> cols) {
		return put_internal(JniUtil.strToBytes(pkey), 
			JniUtil.strToBytes(cols));
	}
	private native boolean put_internal(byte[] pkey, Map<byte[], byte[]> cols);

	@Override public boolean putkeep(byte[] pkey, Map<String, byte[]> cols) {
		return putkeep_internal(pkey,
			JniUtil.strToBytes(cols));
	}
	@Override public boolean putkeep(String pkey, Map<String, String> cols) {
		return putkeep_internal(JniUtil.strToBytes(pkey),
			JniUtil.strToBytes(cols));
	}
	private native boolean putkeep_internal(byte[] pkey, Map<byte[], byte[]> cols);

	@Override public boolean putcat(byte[] pkey, Map<String, byte[]> cols) {
		return putcat_internal(pkey,
			JniUtil.strToBytes(cols));
	}
	@Override public boolean putcat(String pkey, Map<String, String> cols) {
		return putcat_internal(JniUtil.strToBytes(pkey),
			JniUtil.strToBytes(cols));
	}
	private native boolean putcat_internal(byte[] pkey, Map<byte[], byte[]> cols);

	@Override public native boolean out(byte[] pkey);
	@Override public boolean out(String pkey) {
		return out(JniUtil.strToBytes(pkey));
	}

	@Override public native Map<String, byte[]> getCols(byte[] pkey);
	@Override public Map<String, String> getCols(String pkey) {
		final Map<String, byte[]> cols = getCols(JniUtil.strToBytes(pkey));
		return (cols != null ? JniUtil.bytesToStr(cols) : null);
	}

	@Override public native boolean setindex(String name, int type);
	
	@Override public native long genuid();

	@Override public RDBQRY qrynew() {
		RDBQRYJni qry = new RDBQRYJni(this);
		qrys.add(qry);
		return qry;
	}

	@Override public boolean qrydel(RDBQRY qry) {
		return qrys.remove(qry);
	}
	
}
