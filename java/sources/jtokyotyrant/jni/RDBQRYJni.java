package jtokyotyrant.jni;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;

import jtokyotyrant.RDBQRY;

/**
 * @version $Id: RDBQRYJni.java 10 2010-01-05 08:20:53Z koroharo $
 */
public class RDBQRYJni implements RDBQRY {
	
	/** pointer to the native object */
	private long ptr = 0;

	private RDBTBLJni rdbtbl;
	
	static {
		Loader.load();
		init();
	}
	
	private static native void init();

	RDBQRYJni(RDBTBLJni rdbtbl) {
		super();
		this.rdbtbl = rdbtbl;
		initialize(rdbtbl);
	}

	/**
	* Release resources.
	*/
	protected void finalize(){
		destruct();
	}
	
	private native void initialize(RDBTBLJni rdbtbl);
	private native void destruct();
	
	@Override public native void addcond(String name, int op, String expr);
	@Override public native void setorder(String name, int type);
	@Override public native void setlimit(int max, int skip);
	
	@Override public native List<byte[]> search();
	@Override public List<String> search2() {
		return JniUtil.bytesToStr(search());
	}
	
	@Override public native boolean searchout();
	
	@Override public native List<Map<String, byte[]>> searchget();
	@Override public List<Map<String, String>> searchget2() {
		final List<Map<String, byte[]>> byteMapList = searchget();
		final List<Map<String, String>> strMapList = new ArrayList<Map<String, String>>(byteMapList.size());
		for (Map<String, byte[]> byteMap : byteMapList) {
			strMapList.add(JniUtil.bytesToStr(byteMap));
		}
		return strMapList;
	}
	
	@Override public native int searchcount();
	
	@Override public native String hint();
	
	@Override public native List<byte[]> metasearch(RDBQRY[] qrys, int type);
	@Override public List<String> metasearch2(RDBQRY[] qrys, int type) {
		return JniUtil.bytesToStr(metasearch(qrys, type));
	}

	@Override public native List<Map<String, byte[]>> parasearch(RDBQRY[] qrys);
	
	@Override public List<Map<String, String>> parasearch2(RDBQRY[] qrys) {
		final List<Map<String, byte[]>> byteMapList = parasearch(qrys);
		final List<Map<String, String>> strMapList = new ArrayList<Map<String, String>>(byteMapList.size());
		for (Map<String, byte[]> byteMap : byteMapList) {
			strMapList.add(JniUtil.bytesToStr(byteMap));
		}
		return strMapList;
	}
	
	
}
