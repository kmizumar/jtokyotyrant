package jtokyotyrant.jni;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jtokyotyrant.RDB;

/**
 * @version $Id: RDBJni.java 39 2010-01-19 08:22:42Z koroharo $
 */
public class RDBJni implements RDB {

	/** pointer to the native object */
	private long ptr = 0;

	static {
		Loader.load();
		init();
	}

	private static native void init();
	
	public RDBJni() {
		super();
		initialize();
	}
	
	/**
	* Release resources.
	*/
	protected void finalize(){
		destruct();
	}
	
	private native void initialize();
	private native void destruct();
	
	@Override public native String errmsg(int ecode);
	@Override public native int ecode();
	@Override public native boolean tune(double timeoutSec, int opts);
	@Override public native boolean open(String host, int port);
	@Override public native boolean open(String serverExpr);
	@Override public native boolean close();
	@Override public native boolean put(byte[] key, byte[] value);
	@Override public native boolean put(String key, String value);
	@Override public native boolean putkeep(byte[] key, byte[] value);
	@Override public native boolean putkeep(String key, String value);
	@Override public native boolean putcat(byte[] key, byte[] value);
	@Override public native boolean putcat(String key, String value);
	@Override public native boolean putshl(byte[] key, byte[] value, int width);
	@Override public native boolean putshl(String key, String value, int width);
	@Override public native boolean putnr(byte[] key, byte[] value);
	@Override public native boolean putnr(String key, String value);
	@Override public native boolean out(byte[] key);
	@Override public native boolean out(String key);

/*
	@Override public boolean out(byte[] key) {
		return out_internal(key);
	}
	private native boolean out_internal(byte[] key);
	@Override public boolean out(String key) {
		return out_internal(key);
	}
	private native boolean out_internal(String key);
*/
	@Override public native byte[] get(byte[] key);
	@Override public native String get(String key);

	@Override public native int mget(Map<byte[], byte[]> recs);
	
	@Override public int mget2(Map<String, String> recs) {
		final Map<byte[], byte[]> byteMap = JniUtil.strToBytes(recs);
		
		final int count = mget(byteMap);
		if (count > 0) {
			recs.clear();
			recs.putAll(JniUtil.bytesToStr(byteMap));
		}
		return count;
	 }
	
	@Override public native int vsiz(byte[] key);
	@Override public native int vsiz(String key);
	@Override public native boolean iterinit();
	@Override public native byte[] iternext();
	@Override public native String iternext2();
	@Override public native List<byte[]> fwmkeys(byte[] prefix, int max);
	@Override public native List<String> fwmkeys(String prefix, int max);
	
	@Override public native int addint(byte[] key, int num);
	@Override public int addint(String key, int num)  {
		return addint(JniUtil.strToBytes(key), num);
	}
	
	@Override public native double adddouble(byte[] key, double num);
	@Override public double adddouble(String key, double num) {
		return adddouble(JniUtil.strToBytes(key), num);
	}
	
	@Override public native byte[] ext(String name, byte[] key, byte[] value, int opts);
	@Override public byte[] ext(String name, byte[] key, int opts) {
		return ext(name, key, new byte[0], opts);
	}
	@Override public byte[] ext(String name, int opts) {
		return ext(name, new byte[0], new byte[0], opts);
	}
	@Override public byte[] ext(String name, byte[] key, byte[] value) {
		return ext(name, key, value, 0);
	}
	@Override public byte[] ext(String name, byte[] key) {
		return ext(name, key, new byte[0], 0);
	}
	@Override public byte[] ext(String name) {
		return ext(name, new byte[0], new byte[0], 0);
	}

	@Override public native String ext2(String name, String key, String value, int opts);
	@Override public String ext2(String name, String key, int opts) {
		return ext2(name, key, "", opts);
	}
	@Override public String ext2(String name, int opts) {
		return ext2(name, "", "", opts);
	}
	@Override public String ext2(String name, String key, String value) {
		return ext2(name, key, value, 0);
	}
	@Override public String ext2(String name, String key) {
		return ext2(name, key, "", 0);
	}
	@Override public String ext2(String name) {
		return ext2(name, "", "", 0);
	}
	
	@Override public native boolean sync();
	@Override public native boolean optimize(String params);
	@Override public native boolean vanish();
	@Override public native boolean copy(String destfileOrCommand);
	@Override public native boolean restore(String ulogDirectory, long tsMic, int opbs);
	@Override public native boolean setmst(String host, int port, long tsMic, int opbs);
	@Override public native boolean setmst(String serverExpr, long ts, int opbs);
	@Override public native long rnum();
	@Override public native long size();
	@Override public native String stat();
	@Override public native List<byte[]> misc(String funcName, int opts, byte[]... args);
	@Override public List<byte[]> misc(String funcName, byte[]... args) {
		return misc(funcName, 0, args);
	}
	@Override public native List<String> misc(String funcName, int opts, String... args);
	@Override public List<String> misc(String funcName, String... args) {
		return misc(funcName, 0, args);
	}


}
