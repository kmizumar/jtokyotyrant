package jtokyotyrant;

/**
 * jtokyotyrant entry point.
 * <p>
 * This class has some factory methods.
 * </p>
 * <p>
 * <ul>
 * <li>{@link #rdbnew} create a Tokyo Tyrant remote database object.
 * <li>{@link #rdbtblnew} create a Tokyo Tyrant remote database object with table extension.
 * </ul>
 * </p>
 * @version $Id: JTT.java 15 2010-01-05 09:29:22Z koroharo $
 */
public class JTT {
	private JTT() {}
	
	/**
	 * This method is used in order to store a record into a remote database object.
	 * @return new remote database object
	 */
	public static RDB rdbnew() {
		// TODO:
		return new jtokyotyrant.jni.RDBJni();
	}


	/**
	 * This method is used in order to store a record into a remote database object
	 * with table extension.
	 * @return new remote database object
	 */
	public static RDBTBL rdbtblnew() {
		// TODO:
		return new jtokyotyrant.jni.RDBTBLJni();
	}
}
