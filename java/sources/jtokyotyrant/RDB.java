package jtokyotyrant;

import java.util.List;
import java.util.Map;

/**
 * Remote database object.
 * <p>
 * Before store or retrieve records, you need to connect the remote database.
 * The {@link #open(String, int)} and {@link #open(String)} are used to open the
 * database connection. 
 * When this object is no longer use, it should be closed with the {@link #close()}.
 * </p>
 * @version $Id: RDB.java 32 2010-01-08 07:13:48Z koroharo $
 */
public interface RDB {

	/** error code: success */
	int ESUCCESS = 0;
	/** error code: invalid operation */                            
	int EINVALID = 1;
	/** error code: host not found */
	int ENOHOST = 2;
	/** error code: connection refused */
	int EREFUSED = 3;
	/** error code: send error */
	int ESEND = 4;
	/** error code: recv error */
	int ERECV = 5;
	/** error code: existing record */
	int EKEEP = 6;
	/** error code: no record found */
	int ENOREC = 7;
	/** error code: miscellaneous error */
	int EMISC = 9999;


	/**tuning option: reconnect automatically */
	int TRECON = 1 << 0;

	/** scripting extension option: record locking */
	int XOLCKREC = 1 << 0;   
	/** scripting extension option: global locking */               
	int XOLCKGLB = 1 << 1;

	/** restore option: consistency checking*/
	int ROCHKCON = 1 << 0;

	/* miscellaneous operation option: omission of update log */
	int MONOULOG = 1 << 0;

	/**
	 * This method is used in order to get the message string corresponding to 
	 * an error code.
	 * @param ecode specifies the error code.
	 * @return messge string of the error code.
	 */
	String errmsg(int ecode);
	/**
	 * This method is used in order to get the last happened error code of a 
	 * remote database object.
	 * <p>
	 * The following error code is defined: 
	 * <ul>
	 * <li> {@link #ESUCCESS}
	 * <li> {@link #EINVALID}
	 * <li> {@link #ENOHOST}
	 * <li> {@link #EREFUSED}
	 * <li> {@link #ESEND}
	 * <li> {@link #ERECV}
	 * <li> {@link #EKEEP}
	 * <li> {@link #ENOREC}
	 * <li> {@link #EMISC}
	 * </ul>
	 * </p>
	 * @return last happened error code.
	 */
	int ecode();
	/**
	 * This method is used in order to set the tuning parameters of a hash 
	 * database object.
	 * <p>
	 * Note that the tuning parameters should be set before the database 
	 * is opened.
	 * </p>
	 * @param timeoutSec specifies the timeout of each query in seconds. 
	 *            If it is not more than 0, the timeout is not specified.
	 * @param opts specifies options by bitwise-or: {@link #TRECON} specifies 
	 *            that the connection is recovered automatically when it is 
	 *            disconnected.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean tune(double timeoutSec, int opts);
	/**
	 * This method is used in order to open a remote database.
	 * @param host specifies the name or the address of the server.
	 * @param port specifies the port number. If it is not more than 0, 
	 *            UNIX domain socket is used and the path of the socket file is
	 *            specified by the host parameter.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean open(String host, int port);
	/**
	 * This method is used in order to open a remote database with a simple 
	 * server expression.
	 * @param serverExpr specifies the simple server expression. 
	 *            It is composed of two substrings separated by ":". 
	 *            The former field specifies the name or the address of the 
	 *            server. The latter field specifies the port number. 
	 *            If the latter field is omitted, the default port number 1978 
	 +            is specified.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean open(String serverExpr);
	/**
	 * This method is s used in order to close a remote database object.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean close();
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean put(byte[] key, byte[] value);
	/**
	 * This method is used in order to store a string record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean put(String key, String value);
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database,
	 * <ul>	
	 * <li>This method has no effect to the database.
	 * <li>The return value is false.
	 * <li>The {@link #ecode} method returns {@link #EKEEP}
	 * </ul>
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putkeep(byte[] key, byte[] value);
	/**
	 * This method is used in order to store a string record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database,
	 * <ul>	
	 * <li>This method has no effect to the database.
	 * <li>The return value is false.
	 * <li>The {@link #ecode} method returns {@link #EKEEP}
	 * </ul>
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putkeep(String key, String value);
	/**
	 * This method is used in order to concatenate a value at the end of 
	 * the existing record in a remote database object.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putcat(byte[] key, byte[] value);
	/**
	 * This method is used in order to concatenate a string value at the end of 
	 * the existing record in a remote database object.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putcat(String key, String value);
	/**
	 * This method is used in order to concatenate a value at the end of the 
	 * existing record and shift it to the left.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @param width specifes the width of the record.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putshl(byte[] key, byte[] value, int width);
	/**
	 * This method is used in order to concatenate a strign value at the end of the 
	 * existing record and shift it to the left.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @param width specifes the width of the record.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putshl(String key, String value, int width);
	/**
	 * This method is used in order to store a record into a remote database 
	 * object without response from the server.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putnr(byte[] key, byte[] value);
	/**
	 * This method is used in order to store a record into a remote database 
	 * object without response from the server.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param key specifies the key
	 * @param value specfies the value
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putnr(String key, String value);
	/**
	 * This method is used in order to remove a record of a remote database
	 * object.
	 * @param key specifies the key
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean out(byte[] key);
	/**
	 * This method is used in order to remove a record of a remote database
	 * object.
	 * @param key specifies the key
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean out(String key);
	/**
	 * This method is used in order to retrieve a record in a remote database
	 * object.
	 * @param key specifies the key
	 * @return If successful, the return value is the value of the corresponding
	 *         record. If no record corrsponds, the return value is 'null'.
	 */
	byte[] get(byte[] key);
	/**
	 * This method is used in order to retrieve a record in a remote database
	 * object.
	 * @param key specifies the key
	 * @return If successful, the return value is the value of the corresponding
	 *         record. If no record corrsponds, the return value is 'null'.
	 */
	String get(String key);
	/**
	 * This method is used in order to retrieve records in a remote database 
	 * object.
	 * @param recs specifies a map object containing the retrieval keys. 
	 *           As a result of this method, keys existing in the database have 
	 *           the corresponding values and keys not existing in the database 
	 *           are removed.
	 * @return If successful, the return value is the number of retrieved 
	 *         records or -1 on failure.
	 */
	int mget(Map<byte[], byte[]> recs);
	/**
	 * This method is used in order to retrieve records in a remote database 
	 * object.
	 * @param recs specifies a map object containing the retrieval keys. 
	 *           As a result of this method, keys existing in the database have 
	 *           the corresponding values and keys not existing in the database 
	 *           are removed.
	 * @return If successful, the return value is the number of retrieved 
	 *         records or -1 on failure.
	 */
	int mget2(Map<String, String> recs);
	/**
	 * This method is used in order to get the size of the value of a record 
	 * in a remote database object.
	 * @param key specifies the key.
	 * @return If successful, the return value is the size of the value of the 
	 *         corresponding record, else, it is -1 
	 */
	int vsiz(byte[] key);
	/**
	 * This method is used in order to get the size of the value of a record 
	 * in a remote database object.
	 * @param key specifies the key.
	 * @return If successful, the return value is the size of the value of the 
	 *         corresponding record, else, it is -1 
	 */
	int vsiz(String key);
	/**
	 * This method is used in order to initialize the iterator of a remote 
	 * database object.
	 * <p>
	 * The following code shows typical usage.
	 * <pre>
	 * RDB rdb = ...
	 * if (!rdb.iterinit()) {
	 *   ... handle error ...
	 * }
	 * byte[] key = null;
	 * while ((key = rdb.iternext()) != null) {
	 *   byte[] value = rdb.get(key);
	 *   .... Use key and value ....
	 * }
	 * </pre>
	 * </p>
	 * @return The iterator is used in order to access the key of every record 
	 *         stored in a database.
	 */
	boolean iterinit();
	/**
	 * This method is used in order to get the next key of the iterator of a 
	 * remote database object.
	 * <p>You should see {@link #iterinit}.</p>
	 * @return If successful, the return value is the next key, else, 
	 *         it is `null'. `null' is returned when no record is to be get
	 *         out of the iterator.
	 */
	byte[] iternext();
	/**
	 * This method is used in order to get the next key of the iterator of a 
	 * remote database object.
	 * <p>You should see {@link #iterinit}.</p>
	 * @return If successful, the return value is the next key, else, 
	 *         it is `null'. `null' is returned when no record is to be get
	 *         out of the iterator.
	 */
	String iternext2();
	/**
	 * This method is used in order to get forward matching keys in a remote 
	 * database object.
	 * @param prefix specifies the prefix.
	 * @param max specifies the maximum number of keys to be fetched.
	 *           If it is negative, no limit is specified.
	 * @return The return value is a list of the corresponding keys.
	 *         This function does never fail. It returns an empty list even if 
	 *         no key corresponds.
	 */
	List<byte[]> fwmkeys(byte[] prefix, int max);
	/**
	 * This method is used in order to get forward matching keys in a remote 
	 * database object.
	 * @param prefix specifies the prefix.
	 * @param max specifies the maximum number of keys to be fetched.
	 *           If it is negative, no limit is specified.
	 * @return The return value is a list of the corresponding keys.
	 *         This function does never fail. It returns an empty list even if 
	 *         no key corresponds.
	 */
	List<String> fwmkeys(String prefix, int max);
	
	/**
	 * This method is used in order to add an integer to a record in a remote 
	 * database object.
	 * <p>
	 * If the corresponding record exists, the value is treated as an integer
	 * and is added to. 
	 * If no record corresponds, a new record of the additional value is stored.
	 * </p>
	 * @param key specifies the key.
	 * @param num specifies the additional value.
	 * @return If successful, the return value is the summation value, 
	 *         else, it is Integer.MIN_VALUE.
	 */	
	int addint(byte[] key, int num);
	/**
	 * This method is used in order to add an integer to a record in a remote 
	 * database object.
	 * <p>
	 * If the corresponding record exists, the value is treated as an integer
	 * and is added to. 
	 * If no record corresponds, a new record of the additional value is stored.
	 * </p>
	 * @param key specifies the key.
	 * @param num specifies the additional value.
	 * @return If successful, the return value is the summation value, 
	 *         else, it is Integer.MIN_VALUE.
	 */	
	int addint(String key, int num);
	/**
	 * This method is used in order to add a real number to a record in a remote
	 * database object.
	 * <p>
	 * If the corresponding record exists, the value is treated as an integer
	 * and is added to. 
	 * If no record corresponds, a new record of the additional value is stored.
	 * </p>
	 * @param key specifies the key.
	 * @param num specifies the additional value.
	 * @return If successful, the return value is the summation value, 
	 *         else, it is Double.NaN.
	 */	
	double adddouble(byte[] key, double num);
	/**
	 * This method is used in order to add a real number to a record in a remote
	 * database object.
	 * <p>
	 * If the corresponding record exists, the value is treated as an integer
	 * and is added to. 
	 * If no record corresponds, a new record of the additional value is stored.
	 * </p>
	 * @param key specifies the key.
	 * @param num specifies the additional value.
	 * @return If successful, the return value is the summation value, 
	 *         else, it is Double.NaN.
	 */	
	double adddouble(String key, double num);
	/**
	 * This method is used in order to call a function of the script language
	 * extension.
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param value specifies the value.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name, byte[] key, byte[] value, int opts);
	/**
	 * @see #ext(String, byte[], byte[], int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name, byte[] key, int opts);
	/**
	 * @see #ext(String, byte[], byte[], int)
	 * @param name specifies the function name.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name, int opts);
	/**
	 * @see #ext(String, byte[], byte[], int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param value specifies the value.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name, byte[] key, byte[] value);
	/**
	 * @see #ext(String, byte[], byte[], int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name, byte[] key);
	/**
	 * @see #ext(String, byte[], byte[], int)
	 * @param name specifies the function name.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	byte[] ext(String name);
	/**
	 * This method is used in order to call a function of the script language
	 * extension.
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param value specifies the value.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name, String key, String value, int opts);
	/**
	 * @see #ext2(String, String, String, int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name, String key, int opts);
	/**
	 * @see #ext2(String, String, String, int)
	 * @param name specifies the function name.
	 * @param opts specifies options by bitwise-or: {@link #XOLCKREC} for 
	 *           record locking, {@link #XOLCKGLB} for global locking.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name, int opts);
	/**
	 * @see #ext2(String, String, String, int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @param value specifies the value.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name, String key, String value);
	/**
	 * @see #ext2(String, String, String, int)
	 * @param name specifies the function name.
	 * @param key specifies tye key.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name, String key);
	/**
	 * @see #ext2(String, String, String, int)
	 * @param name specifies the function name.
	 * @return If successful, the return value is the value of the response.
	 *         `null' is returned on failure.
	 */
	String ext2(String name);
	
	/**
	 * This method is used in order to synchronize updated contents of a remote
	 * database object with the file and the device.
	 * @return If successful, the return value is true, else, it is false.
	 */	
	boolean sync();
/*
	Tuning parameters can trail the name, separated by "#".
	Each parameter is composed of the name and the value, separated by "=".  
	
	On-memory hash database supports "bnum", "capnum", and "capsiz".
	On-memory tree database supports "capnum" and "capsiz".
	Hash database supports "mode", "bnum", "apow", "fpow", "opts", "rcnum", "xmsiz", and "dfunit".
	B+ tree database supports "mode", "lmemb", "nmemb", "bnum", "apow", "fpow", "opts", "lcnum", "ncnum", "xmsiz", and "dfunit".
	Fixed-length database supports "mode", "width", and "limsiz".
	Table database supports "mode", "bnum", "apow", "fpow", "opts", "rcnum", "lcnum", "ncnum", "xmsiz", "dfunit", and "idx".

   If successful, the return value is true, else, it is false.
   The tuning parameter 
   "capnum" ? specifies the capacity number of records.
   "capsiz" ? specifies the capacity size of using memory. Records spilled the capacity are removed by the storing order.
   "mode" ? can contain "w" of writer, "r" of reader, "c" of creating, "t" of truncating, "e" of no locking, and "f" of non-blocking lock.  The default mode is relevant to "wc".
   "opts" ? can contains "l" of large option, "d" of Deflate option, "b" of BZIP2 option, and "t" of TCBS option.

   "idx" A specifies the column name of an index and its type separated by ":".
	   `name' specifies the name of a column.  If the name of an existing index is specified, the
	   index is rebuilt.  An empty string means the primary key.
	   `type' specifies the index type: `TDBITLEXICAL' for lexical string, `TDBITDECIMAL' for decimal
	   string, `TDBITTOKEN' for token inverted index, `TDBITQGRAM' for q-gram inverted index.  If it
	   is `TDBITOPT', the index is optimized.  If it is `TDBITVOID', the index is removed.  If
	   `TDBITKEEP' is added by bitwise-or and the index exists, this function merely returns failure.

from tchdb.h
   `bnum' ? specifies the number of elements of the bucket array.  If it is not more than 0, the default value is specified.  The default value is two times of the number of records.
   `apow' ? specifies the size of record alignment by power of 2.  If it is negative, the current setting is not changed.
   `fpow' ? specifies the maximum number of elements of the free block pool by power of 2.  If it is negative, the current setting is not changed.
   `opts' ? specifies options by bitwise-or: `HDBTLARGE' specifies that the size of the database can be larger than 2GB by using 64-bit bucket array, `HDBTDEFLATE' specifies that each record is compressed with Deflate encoding, `HDBTBZIP' specifies that each record is compressed with BZIP2 encoding, `HDBTTCBS' specifies that each record is compressed with TCBS encoding.  If it is `UINT8_MAX', the current setting is not changed.
   `dfunit' B specifie the unit step number.  If it is not more than 0, the auto defragmentation is disabled.  It is disabled by default. Note that the defragmentation parameters should be set before the database is opened.
   `rcnum' B specifies the maximum number of records to be cached.  If it is not more than 0, the record cache is disabled.  It is disabled by default. Note that the caching parameters should be set before the database is opened.
   `xmsiz' B specifies the size of the extra mapped memory.  If it is not more than 0, the extra mapped memory is disabled.  The default size is 67108864. Note that the mapping parameters should be set before the database is opened.

from tcbdb.h
   `lmemb' ? specifies the number of members in each leaf page.  If it is not more than 0, the current setting is not changed.
   `nmemb' ? specifies the number of members in each non-leaf page.  If it is not more than 0, the current setting is not changed.
   `bnum' ? specifies the number of elements of the bucket array.  If it is not more than 0, the default value is specified.  The default value is two times of the number of pages.
   `apow' ? specifies the size of record alignment by power of 2.  If it is negative, the current setting is not changed.
   `fpow' ? specifies the maximum number of elements of the free block pool by power of 2.  If it is negative, the current setting is not changed.
   `opts' ? specifies options by bitwise-or: `BDBTLARGE' specifies that the size of the database can be larger than 2GB by using 64-bit bucket array, `BDBTDEFLATE' specifies that each record is compressed with Deflate encoding, `BDBTBZIP' specifies that each page is compressed with BZIP2 encoding, `BDBTTCBS' specifies that each page is compressed with TCBS encoding.  If it is `UINT8_MAX', the current setting is not changed.

   `dfunit' B specifie the unit step number.  If it is not more than 0, the auto defragmentation is disabled.  It is disabled by default. Note that the defragmentation parameter should be set before the database is opened.
   `lcnum' B specifies the maximum number of leaf nodes to be cached.  If it is not more than 0, the default value is specified.  The default value is 1024.
   `ncnum' B specifies the maximum number of non-leaf nodes to be cached.  If it is not more than 0, the default value is specified.  The default value is 512.
   `xmsiz' B specifies the size of the extra mapped memory.  If it is not more than 0, the extra mapped memory is disabled.  It is disabled by default.

from tcfdb.h
   `width' ? specifies the width of the value of each record.  If it is not more than 0, the current setting is not changed.
   `limsiz' ? specifies the limit size of the database file.  If it is not more than 0, the current setting is not changed.

from tctdb.h
   `bnum' ? specifies the number of elements of the bucket array.  If it is not more than 0, the default value is specified.  The default value is two times of the number of records.
   `apow' ? specifies the size of record alignment by power of 2.  If it is negative, the current setting is not changed.
   `fpow' ? specifies the maximum number of elements of the free block pool by power of 2.  If it is negative, the current setting is not changed.
   `opts' ? specifies options by bitwise-or: `BDBTLARGE' specifies that the size of the database can be larger than 2GB by using 64-bit bucket array, `BDBTDEFLATE' specifies that each record is compressed with Deflate encoding, `BDBTBZIP' specifies that each record is compressed with BZIP2 encoding, `BDBTTCBS' specifies that each record is compressed with TCBS encoding.  If it is `UINT8_MAX', the current setting is not changed.
   `rcnum' B specifies the maximum number of records to be cached.  If it is not more than 0, the record cache is disabled.  It is disabled by default.
   `lcnum' B specifies the maximum number of leaf nodes to be cached.  If it is not more than 0, the default value is specified.  The default value is 4096.
   `ncnum' B specifies the maximum number of non-leaf nodes to be cached.  If it is not more than 0, the default value is specified.  The default value is 512.
   `xmsiz' B specifies the size of the extra mapped memory.  If it is not more than 0, the extra mapped memory is disabled.  The default size is 67108864.
   `dfunit' B specifie the unit step number.  If it is not more than 0, the auto defragmentation is disabled.  It is disabled by default.


       Note that the caching parameters should be set before the database is opened.
       Leaf nodes and non-leaf nodes are used in column indices.

      This function is useful to reduce the size of the database file with data fragmentation by successive updating.


   For example, "#bnum=1000000#opts=ld" means that the name of the database file is
   "casket.tch", and the bucket number is 1000000, and the options are large and Deflate. 
*/
	/**
	 * This method is used in order to optimize the storage of a remove database
	 * object.
	 * <p>
	 * Tuning parameter is composed of the name and the value, separated by "=".
	 * Several parameters can be used separated by "#".
	 * <pre>
	 * Example:
	 *     bnum=100000#capnum=200000
	 * </pre>
	 * <ul>
	 * <li>On-memory hash database supports "bnum", "capnum", and "capsiz".
	 * <li>On-memory tree database supports "capnum" and "capsiz".
	 * <li>Hash database supports "bnum", "apow", "fpow", "opts"
	 * <li>B+ tree database supports "lmemb", "nmemb", "bnum", "apow", "fpow", "opts"
	 * <li>Fixed-length database supports "width", and "limsiz".
	 * <li>Table database supports "bnum", "apow", "fpow", "opts"
	 * </ul>
	 * <p>
	 * The following list is the description of the tuning parameters.
	 * <ul>
	 * <li>"capnum" specifies the capacity number of records.
	 * <li>"capsiz" specifies the capacity size of using memory. Records spilled the capacity are removed by the storing order.
	 * <li>"opts" can contains "l" of large option, "d" of Deflate option, "b" of BZIP2 option, and "t" of TCBS option.
	 * <li>"idx" specifies the column name of an index and its type separated by ":". 
	 *    <ul>
	 *      <li>"name" specifies the name of a column.  If the name of an 
	 *          existing index is specified, the index is rebuilt. 
	 *          An empty string means the primary key.
	 *      <li>"type" specifies the index type: 
	 *          {@link RDBTBL#ITLEXICAL} for lexical string,
	 *          {@link RDBTBL#ITDECIMAL} for decimal string,
	 *          {@link RDBTBL#ITTOKEN} for token inverted index,
	 *          {@link RDBTBL#ITQGRAM} for q-gram inverted index.
	 *          If it is {@link RDBTBL#ITOPT}, the index is optimized.
	 *          If it is {@link RDBTBL#ITVOID}, the index is removed.
	 *          If {@link RDBTBL#ITKEEP} is added by bitwise-or and the index 
	 *          exists, this function merely returns failure.
	 *    </ul>
	 * <li>"bnum" specifies the number of elements of the bucket array.  If it is not more than 0, the default value is specified.  The default value is two times of the number of records or pages.
	 * <li>"apow" specifies the size of record alignment by power of 2.  If it is negative, the current setting is not changed.
	 * <li>"fpow" specifies the maximum number of elements of the free block pool by power of 2.  If it is negative, the current setting is not changed.
	 * <li>"lmemb" specifies the number of members in each leaf page.  If it is not more than 0, the current setting is not changed.
	 * <li>"nmemb" specifies the number of members in each non-leaf page.  If it is not more than 0, the current setting is not changed.
	 * <li>"width" specifies the width of the value of each record.  If it is not more than 0, the current setting is not changed.
	 * <li>"limsiz" specifies the limit size of the database file.  If it is not more than 0, the current setting is not changed.
	 * </p>
	 * @param params specifies the string of the tuning parameters.
	 *           If it is `null', it is not used.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean optimize(String params);
	
	/**
	 * This method is used in order to remove all records of a remote database 
	 * object.
	 * @return if successful, the return value is true, else, it is false.
	 */
	boolean vanish();
	/**
	 * This method is used in order to copy the database file of a remote database
	 * object.
	 * <p>
	 * The database file is assured to be kept synchronized and not modified 
	 * while the copying or executing operation is in progress.
	 * So, this function is useful to create a backup file of the database file.
	 * </p>
	 * @param destfileOrCommand specifies the path of the destination file. 
	 *           If it begins with `@', the trailing substring is executed as a 
	 *           command line.
	 * @return If successful, the return value is true, else, it is false.
	 *         False is returned if the executed command returns non-zero code.
	 */
	boolean copy(String destfileOrCommand);
	/**
	 * This method is used in order to restore the database file of a remote 
	 * database object from the update log.
	 * @param ulogDirectory specifies the path of the update log directory.
	 * @param tsMic specifies the beginning time stamp in microseconds.
	 * @param opts specifies options by bitwise-or: {@link #ROCHKCON} for consistency checking.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean restore(String ulogDirectory, long tsMic, int opts);
	/**
	 * This method is used in order to set the replication master of a remote 
	 * database object.
	 * @param host specifies the name or the address of the server. If it is `NULL', replication of the database is disabled.
	 * @param port specifies the port number.
	 * @param tsMic specifies the beginning timestamp in microseconds.
	 * @param opts specifies options by bitwise-or: {@link #ROCHKCON} for consistency checking.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean setmst(String host, int port, long tsMic, int opts);
	/**
	 * This method is used in order to set the replication master of a remote 
	 * database object.
	 * @param serverExpr specifies the simple server expression. It is composed 
	 *           of two substrings separated by ":". The former field specifies
	 *           the name or the address of the server. The latter field 
	 *           specifies the port number. If the latter field is omitted, 
	 *           the default port number is specified.
	 * @param tsMic specifies the beginning timestamp in microseconds.
	 * @param opts specifies options by bitwise-or: {@link #ROCHKCON} for consistency checking.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean setmst(String serverExpr, long tsMic, int opts);
	/**
	 * This method is used in order to get the number of records of a remote 
	 * database object.
	 * @return The return value is the number of records or 0 if the object does
	 *         not connect to any database server.
	 */
	long rnum();
	/**
	 * This method is used in order to get the size of the database of a remote
	 * database object.
	 * @return The return value is the size of the database or 0 if the object 
	 *         does not connect to any database server.
	 */
	long size();
	/**
	 * This method is used in order to get the status string of the database of 
	 * a remote database object.
	 * <p>
	 * The message format is TSV. The first field of each line means the parameter
	 * name and the second field means the value.
	 * </p>
	 * @return The return value is the status message of the database or 
	 *         `null' if the object does not connect to any database server. 
	 */
	String stat();
	/**
	 * @see #misc(String, int, byte[][])
	 * @param funcName 	specifies the name of the function.
	 * @param args specifies function arguments.
	 * @return If successful, the return value is a list object of the result.
	 *        `null' is returned on failure.
	 */
	List<byte[]> misc(String funcName, byte[]... args);
	/**
	 * This method is used in order to call a versatile function for 
	 * miscellaneous operations of a remote database object.
	 * <p>
	 * The following list shows all functions supported by all databases.
	 * <ul>
	 * <li>"put" is to store a record. It receives a key and a value, and returns an empty list.
	 * <li>"out" is to remove a record. It receives a key, and returns an empty list.
	 * <li>"get" is to retrieve a record. It receives a key, and returns a list of the values. 
	 * <li>"putlist" is to store records. It receives keys and values one after the other, and returns an empty list.
	 * <li>"outlist" is to remove records. It receives keys, and returns an empty list.
	 * <li>"getlist" is to retrieve records. It receives keys, and returns keys and values of corresponding records one after the other.
	 * </ul>
	 * </p>
	 * @param funcName 	specifies the name of the function.
	 * @param opts specifies options by bitwise-or: {@link #MONOULOG} for 
	 *           omission of the update log.
	 * @param args specifies function arguments.
	 * @return If successful, the return value is a list object of the result.
	 *        `null' is returned on failure.
	 */
	List<byte[]> misc(String funcName, int opts, byte[]... args);
	/**
	 * @see #misc(String, int, byte[][])
	 * @param funcName 	specifies the name of the function.
	 * @param args specifies function arguments.
	 * @return If successful, the return value is a list object of the result.
	 *        `null' is returned on failure.
	 */
	List<String> misc(String funcName, String... args);
	/**
	 * @see #misc(String, int, byte[][])
	 * @param funcName 	specifies the name of the function.
	 * @param opts specifies options by bitwise-or: {@link #MONOULOG} for 
	 *           omission of the update log.
	 * @param args specifies function arguments.
	 * @return If successful, the return value is a list object of the result.
	 *        `null' is returned on failure.
	 */
	List<String> misc(String funcName, int opts, String... args);
}
