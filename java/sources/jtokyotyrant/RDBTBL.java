package jtokyotyrant;

import java.util.Map;

/**
 * Remote database object with table extension.
 * @see RDB
 * @version $Id: RDBTBL.java 35 2010-01-19 08:07:53Z koroharo $
 */
public interface RDBTBL extends RDB {
	/** index type: lexical string */
	int ITLEXICAL  	=  	0;
	/** index type: decimal string */
	int ITDECIMAL 	= 	1;
	/** index type: token inverted index */
	int ITTOKEN 	= 	2;
	/** index type: q-gram inverted index */
	int ITQGRAM 	= 	3;
	/** index type: optimize */
	int ITOPT 	= 	9998;
	/** index type: void */
	int ITVOID 	= 	9999;
	/** index type: keep existing index */
	int ITKEEP 	= 	1 << 24;
	
	/**
	 * This method is s used in order to close a remote database object.
	 * @return If successful, the return value is true, else, it is false.
	 */
	@Override boolean close();
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean put(byte[] pkey, Map<String, byte[]> cols);
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database, it is overwritten.
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean put(String pkey, Map<String, String> cols);
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database,
	 * <ul>	
	 * <li>This method has no effect to the database.
	 * <li>The return value is false.
	 * <li>The {@link #ecode} method returns {@link #EKEEP}
	 * </ul>
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putkeep(byte[] pkey, Map<String, byte[]> cols);
	/**
	 * This method is used in order to store a record into a remote database object.
	 * <p>
	 * If a record with the same key exists in the database,
	 * <ul>	
	 * <li>This method has no effect to the database.
	 * <li>The return value is false.
	 * <li>The {@link #ecode} method returns {@link #EKEEP}
	 * </ul>
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putkeep(String pkey, Map<String, String> cols);
	/**
	 * This method is used in order to concatenate columns of the existing record
	 * in a remote database object.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putcat(byte[] pkey, Map<String, byte[]> cols);
	/**
	 * This method is used in order to concatenate columns of the existing record
	 * in a remote database object.
	 * <p>
	 * If there is no corresponding record, a new record is created.
	 * </p>
	 * @param pkey specifies the primary key
	 * @param cols specfies a map object containing columns
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean putcat(String pkey, Map<String, String> cols);
	/**
	 * This method is used in order to remove a record of a remote database
	 * object.
	 * @param pkey specifies the primary key
	 * @return If successful, the return value is true, else, it is false.
	 */
	@Override boolean out(byte[] pkey);
	/**
	 * This method is used in order to remove a record of a remote database
	 * object.
	 * @param pkey specifies the primary key
	 * @return If successful, the return value is true, else, it is false.
	 */
	@Override boolean out(String pkey);
	/**
	 * This method is used in order to retrieve a record in a remote database
	 * object.
	 * @param pkey specifies the primary key
	 * @return If successful, the return value is a map object of the columns of
	 *         the corresponding record.
	 *         If no record corrsponds, the return value is 'null'.
	 */
	Map<String, byte[]> getCols(byte[] pkey);
	/**
	 * This method is used in order to retrieve a record in a remote database
	 * object.
	 * @param pkey specifies the primary key
	 * @return If successful, the return value is a map object of the columns of
	 *         the corresponding record.
	 *         If no record corrsponds, the return value is 'null'.
	 */
	Map<String, String> getCols(String pkey);
	/**
	 * This method is used in order to set a column index to a remote database 
	 * object.
	 * <p>
	 * Index types are listed as below:
	 * <ul>
	 * <li>{@link #ITLEXICAL} for lexical string.
	 * <li>{@link #ITDECIMAL} for decimal string.
	 * <li>{@link #ITTOKEN} for token inverted index.
	 * <li>{@link #ITQGRAM} for q-gram inverted index.
	 * <li>If it is {@link #ITOPT}, the index is optimized. 
	 * <li>If it is {@link #ITVOID}, the index is removed. 
	 * <li>If {@link #ITKEEP} is added by bitwise-or and the index exists, 
	 *     this function merely returns failure.
	 * <li>
	 * </ul>
	 * </p>
	 * @param name specifies the name of a column.
	 *           If the name of an existing index is specified, the index is 
	 *           rebuilt. An empty string means the primary key.
	 * @param type specifies the index type
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean setindex(String name, int type);
	/**
	 * This method is used in order to generate a unique ID number of a remote 
	 * database object.
	 * @return The return value is the new unique ID number or -1 on failure.
	 */
	long genuid();
	/**
	 * This method is used in order to create a query object.
	 * @return The return value is the new query object.
	 */	
	RDBQRY qrynew();
	/**
	 * This method is used in order to delete a query object.
	 * @param qry specifies the query object.
	 * @return If successful, the return value is true, else, it is false.
	 */
	boolean qrydel(RDBQRY qry);
}
